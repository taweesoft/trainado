package GUI;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Map {
	
	private ArrayList<JLabel> destinationArr = new ArrayList<JLabel>();
	protected JFrame frame;
	ClassLoader loader = this.getClass().getClassLoader();
	private JLabel label_back;
	public Map() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.setUndecorated(true);
		frame.setResizable(false);
		frame.setBounds(0, 0, 504, 606);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 504, 608);
		panel.setLayout(null);
		frame.getContentPane().add(panel);
		
		JLabel label = new JLabel();
		label.setBounds(135, 64, 40, 40);
		panel.add(label);
		
		JLabel label_1 = new JLabel();
		label_1.setBounds(135, 151, 40, 40);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel();
		label_2.setBounds(366, 151, 40, 40);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel();
		label_3.setBounds(156, 243, 40, 40);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel();
		label_4.setBounds(74, 449, 40, 40);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel();
		label_5.setBounds(227, 558, 40, 40);
		panel.add(label_5);
		
		destinationArr.add(label); //0 Chiangmai
		destinationArr.add(label_1); //1 Kampanpetch
		destinationArr.add(label_2); //2 Konkean
		destinationArr.add(label_3); //3 Bangkok
		destinationArr.add(label_4); //4 Huahin
		destinationArr.add(label_5);
		
		label_back = new JLabel(new ImageIcon(loader.getResource("images/back_1.png")));
		label_back.setBounds(10, 491, 93, 115);
		label_back.addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				Main.frame.setVisible(true);
				frame.setVisible(false);
			}
			public void mouseClicked(MouseEvent e){}
			public void mouseEntered(MouseEvent e){
				label_back.setIcon(new ImageIcon(loader.getResource("images/back_2.gif")));
			}
			public void mouseExited(MouseEvent e){
				label_back.setIcon(new ImageIcon(loader.getResource("images/back_1.png")));
			}
			public void mousePressed(MouseEvent e){}
		});
		panel.add(label_back);
		JLabel lb_thaimap = new JLabel(new ImageIcon(Map.class.getResource("/images/thai_map.gif")));
		lb_thaimap.setBounds(0, 0, 504, 606);
		panel.add(lb_thaimap);
		addlabelEvent();
		
	}
	
	PointerInfo pointinfo = MouseInfo.getPointerInfo();
	Point mouse = pointinfo.getLocation();
	public void addlabelEvent(){
		destinationArr.get(0).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				showTimeTable(1);
			}
			public void mouseEntered(MouseEvent e){
			}
			public void mouseExited(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
		});
		destinationArr.get(1).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				showTimeTable(2);
			}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
		});
		destinationArr.get(2).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				showTimeTable(3);
			}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
		});
		destinationArr.get(4).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				showTimeTable(4);
			}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
		});
		destinationArr.get(5).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				showTimeTable(5);
			}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
		});
		
		
	}
	public void showTimeTable(int diff){
		TimeTable time_main = new TimeTable(diff);
		time_main.frame.setVisible(true);
		frame.setVisible(false);
	}

}
