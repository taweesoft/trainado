package GUI;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import SourceCode.Person;

public class ShowCustomerList {

	protected JFrame frame;
	private JTable table;
	String[] tableHeader = {"Custumer ID","Firstname","Lastname","Telephone","Email","Gender","Class"};
	
	public ShowCustomerList() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Customer list");
		frame.setBounds(100, 100, 665, 300);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JPopupMenu popup = new JPopupMenu();
		JMenuItem menuItem_showTicket = new JMenuItem("Show ticket list");
		menuItem_showTicket.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				ShowTicketList showticket_main = new ShowTicketList(Person.getCustomerList().get(table.getSelectedRow()),1);
				showticket_main.frame.setVisible(true);
			}
		});
		popup.add(menuItem_showTicket);
		table = new JTable(getCustomerList(),tableHeader);
		table.setComponentPopupMenu(popup);
		JScrollPane scrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 629, 239);
		frame.getContentPane().add(scrollPane);
		
	}
	public String[][] getCustomerList(){
		String[][] customerArr = new String[Person.getCustomerList().size()][1];
		for(int i=0;i<customerArr.length;i++){
			customerArr[i] = Person.getCustomerList().get(i).toString().split(",");
		}
		return customerArr;
	}

}
