package GUI;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import SourceCode.Database;


public class ShowRecent {

	protected JFrame frame;
	private JTable table;

	public ShowRecent() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Recent");
		frame.setBounds(100, 100, 811, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		
		String[] tableHeader = {"Firstname","Lastname","Flight","From","To","Time","Seat type","Seat NO","Price","Status"};
		table = new JTable(getRecent(),tableHeader);
		
		JScrollPane scrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 775, 239);
		frame.getContentPane().add(scrollPane);
	}
	public String[][] getRecent(){
		String[][] recentArr = new String[Database.getRecentArr().size()][6];
		for(int i=0;i<recentArr.length;i++){
			recentArr[i] = Database.getRecentArr().get(i);
		}
		return recentArr;
	}
}
