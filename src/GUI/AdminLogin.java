package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import SourceCode.Database;


public class AdminLogin {

	protected JFrame frame;
	private JTextField txt_username;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JPasswordField txt_password;

	public AdminLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Administrator");
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 373, 211);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txt_password = new JPasswordField();
		txt_password.setBounds(99, 104, 153, 20);
		frame.getContentPane().add(txt_password);
		
		txt_username = new JTextField();
		txt_username.setBounds(99, 60, 153, 20);
		frame.getContentPane().add(txt_username);
		txt_username.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("User Name:");
		lblNewLabel.setBounds(99, 45, 153, 14);
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Password:");
		lblNewLabel_1.setBounds(99, 83, 153, 20);
		frame.getContentPane().add(lblNewLabel_1);
		
		ClassLoader loader = this.getClass().getClassLoader();
		URL url = loader.getResource("images/logo_adminlogin.png");
		ImageIcon logo = new ImageIcon(url);
		lblNewLabel_2 = new JLabel(logo);
		lblNewLabel_2.setBounds(0, 0, 357, 43);
		frame.getContentPane().add(lblNewLabel_2);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<String[]> admin = Database.getAdminArr();
				for(int i=0;i<admin.size();i++){
					if(txt_username.getText().equalsIgnoreCase(admin.get(i)[1]) &&
							txt_password.getText().equals(admin.get(i)[2])){
						AdminStore adminstore = new AdminStore();
						adminstore.frame.setVisible(true);
						frame.setVisible(false);
					}
				}
			}
		});
		btnLogin.setBounds(73, 135, 89, 23);
		frame.getContentPane().add(btnLogin);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		btnCancel.setBounds(197, 135, 89, 23);
		frame.getContentPane().add(btnCancel);
	}
}
