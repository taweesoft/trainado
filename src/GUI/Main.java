package GUI;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import SourceCode.CreateFlight;
import SourceCode.Database;


public class Main {

	protected static JFrame frame;
	CreateFlight createFlight = new CreateFlight();
	Database database = new Database();
	ClassLoader loader = this.getClass().getClassLoader();
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_close;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
		Database.getAllSeatLayoutSQL();
		Database.saveInitialSeatLayout();
		MonitorController mother_main = new MonitorController();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		
		frame = new JFrame();
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		
		frame.setResizable(false);
		frame.setUndecorated(true);
		frame.setBounds(0, 0, 848, 444);
		frame.setLocation(dimension.width/2-frame.getSize().width/2, dimension.height/2-frame.getSize().height/2);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		label = new JLabel(new ImageIcon(loader.getResource("images/book1.png")));
		label.setBounds(510, 160, 210, 71);
		label.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent e){
				label.setIcon(new ImageIcon(loader.getResource("images/book2.gif")));
			}
			public void mouseReleased(MouseEvent e){
				Map map_main = new Map();
				map_main.frame.setVisible(true);
				frame.setVisible(false);
			}
			public void mouseExited(MouseEvent e){
				label.setIcon(new ImageIcon(loader.getResource("images/book1.png")));
			}
		});
		
		label_1 = new JLabel(new ImageIcon(loader.getResource("images/find1.png")));
		label_1.setBounds(510, 242, 210, 71);
		label_1.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				Search searchticket_main = new Search(1);
				searchticket_main.frame.setVisible(true);
			}
			public void mouseEntered(MouseEvent e){
				label_1.setIcon(new ImageIcon(loader.getResource("images/find2.gif")));
			}
			public void mouseExited(MouseEvent e){
				label_1.setIcon(new ImageIcon(loader.getResource("images/find1.png")));
			}
		});
		frame.getContentPane().add(label_1);
		
		label_2 = new JLabel(new ImageIcon(loader.getResource("images/administrator1.png")));
		label_2.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				AdminLogin adminlogin_main = new AdminLogin();
				adminlogin_main.frame.setVisible(true);
				frame.setVisible(false);
			}
			public void mouseEntered(MouseEvent e){
				label_2.setIcon(new ImageIcon(loader.getResource("images/administrator2.gif")));
			}
			public void mouseExited(MouseEvent e){
				label_2.setIcon(new ImageIcon(loader.getResource("images/administrator1.png")));
			}
		});
		label_2.setBounds(464, 301, 250, 83);
		frame.getContentPane().add(label_2);
		frame.getContentPane().add(label);
				
		label_close = new JLabel(new ImageIcon(loader.getResource("images/close.png")));
		label_close.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				System.exit(0);
			}
		});
		label_close.setBounds(809, 11, 29, 29);
		frame.getContentPane().add(label_close);
				
						//JLabel label_mainPic = new JLabel("");
		JLabel label_mainPic = new JLabel(new ImageIcon(loader.getResource("images/main.gif")));
		label_mainPic.setBounds(0, 0, 848, 444);
						
		frame.getContentPane().add(label_mainPic);
	}
}

