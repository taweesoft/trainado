package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import SourceCode.Database;


public class MonitorController {

	protected static JFrame frame;
	private ArrayList<String[]> bookedList = new ArrayList<String[]>();
	private String[] tableHeader = {"Flight","Seat NO"};
	private JPanel panel_flight1P1;
	public static ArrayList<JLabel> labelArr = new ArrayList<JLabel>();
	private JPanel panel_flight1B1;
	private JPanel panel_flight1B2;
	private JPanel panel_flight1N1;
	private JPanel panel_flight1N2;
	private JPanel panel_flight2P1;
	private JPanel panel_flight2P2;
	private JPanel panel_flight2B1;
	private JPanel panel_flight2B2;
	private JPanel panel_flight2N1;
	private JPanel panel_flight2N2;
	private JPanel panel_flight3P1;
	private JPanel panel_flight3P2;
	private JPanel panel_flight3B1;
	private JPanel panel_flight3B2;
	private JPanel panel_flight3N1;
	private JPanel panel_flight3N2;
	private JPanel panel_flight4P1;
	private JPanel panel_flight4P2;
	private JPanel panel_flight4B1;
	private JPanel panel_flight4B2;
	private JPanel panel_flight4N1;
	private JPanel panel_flight4N2;
	private JPanel panel_flight5P1;
	private JPanel panel_flight5P2;
	private JPanel panel_flight5B1;
	private JPanel panel_flight5B2;
	private JPanel panel_flight5N1;
	private JPanel panel_flight5N2;
	private JPanel panel_flight1;
	private JPanel panel_flight2;
	private JPanel panel_flight3;
	private JPanel panel_flight4;
	private JPanel panel_flight5;
	private JPanel panel_6;
	private JLabel lblFlightBangkokchiangmai;
	private JLabel lblFlightBangkokchiangmai_1;
	private JLabel lblFlightBangkokkampanpetch;
	private JPanel panel_flight6;
	private JPanel panel_flight6P1;
	private JPanel panel_flight6P2;
	private JPanel panel_flight6B1;
	private JPanel panel_flight6B2;
	private JPanel panel_flight6N1;
	private JPanel panel_flight6N2;
	private JLabel lblFlightBangkokkampanpetch_1;
	private JLabel lblFlightBangkokkampanpetch_2;
	private JPanel panel_flight7;
	private JPanel panel_flight7P1;
	private JPanel panel_flight7P2;
	private JPanel panel_flight7B1;
	private JPanel panel_flight7B2;
	private JPanel panel_flight7N1;
	private JPanel panel_flight7N2;
	private JLabel lblFlightBangkokkhonKaen;
	private JPanel panel_flight8;
	private JPanel panel_flight8P1;
	private JPanel panel_flight8P2;
	private JPanel panel_flight8B1;
	private JPanel panel_flight8B2;
	private JPanel panel_flight8N1;
	private JPanel panel_flight8N2;
	private JLabel lblFlightBangkokkhonKaen_1;
	private JPanel panel_flight9;
	private JPanel panel_flight9P1;
	private JPanel panel_flight9P2;
	private JPanel panel_flight9B1;
	private JPanel panel_flight9B2;
	private JPanel panel_flight9N1;
	private JPanel panel_flight9N2;
	private JLabel lblFlightBangkokkhonKaen_2;
	private JPanel panel_flight10;
	private JPanel panel_flight10P1;
	private JPanel panel_flight10P2;
	private JPanel panel_flight10B1;
	private JPanel panel_flight10B2;
	private JPanel panel_flight10N1;
	private JPanel panel_flight10N2;
	private JLabel lblFlightBangkokhuaHin;
	private JPanel panel_flight11;
	private JPanel panel_flight11P1;
	private JPanel panel_flight11P2;
	private JPanel panel_flight11B1;
	private JPanel panel_flight11B2;
	private JPanel panel_flight11N1;
	private JPanel panel_flight11N2;
	private JLabel lblFlightBangkokhuaHin_1;
	private JPanel panel_flight12;
	private JPanel panel_flight12P1;
	private JPanel panel_flight12P2;
	private JPanel panel_flight12B1;
	private JPanel panel_flight12B2;
	private JPanel panel_flight12N1;
	private JPanel panel_flight12N2;
	private JLabel lblFlightBangkokhuaHin_2;
	private JPanel panel_flight13;
	private JPanel panel_flight13P1;
	private JPanel panel_flight13P2;
	private JPanel panel_flight13B1;
	private JPanel panel_flight13B2;
	private JPanel panel_flight13N1;
	private JPanel panel_flight13N2;
	private JLabel lblFlightBangkokhatYai;
	private JPanel panel_flight14;
	private JPanel panel_flight14P1;
	private JPanel panel_flight14P2;
	private JPanel panel_flight14B1;
	private JPanel panel_flight14B2;
	private JPanel panel_flight14N1;
	private JPanel panel_flight14N2;
	private JLabel lblFlightBangkokhatYai_1;
	private JPanel panel_flight15;
	private JPanel panel_flight15P1;
	private JPanel panel_flight15P2;
	private JPanel panel_flight15B1;
	private JPanel panel_flight15B2;
	private JPanel panel_flight15N1;
	private JPanel panel_flight15N2;
	private JLabel lblFlightBangkokhatYai_2;
	private JScrollPane scrollPane_1;
	private JTable table;
	private DefaultTableModel model;
	
	public MonitorController() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Monitor Controller");
		frame.setBounds(100, 100, 1017, 556);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2 - frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		panel_6 = new JPanel();
		panel_6.setMinimumSize(new Dimension(3200, 300));
		panel_6.setPreferredSize(new Dimension(3200,495));
		panel_6.setLayout(null);
		
		panel_flight1 = new JPanel();
		panel_flight1.setBounds(10, 11, 625, 142);
		panel_6.add(panel_flight1);
		panel_flight1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Flight1 TS780 Bangkok-Chiangmai");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(170, 11, 285, 14);
		panel_flight1.add(lblNewLabel);
		
		panel_flight1P1 = new JPanel();
		panel_flight1P1.setBounds(10, 36, 92, 98);
		panel_flight1.add(panel_flight1P1);
		panel_flight1P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		JPanel panel_flight1P2 = new JPanel();
		panel_flight1P2.setBounds(112, 36, 92, 98);
		panel_flight1.add(panel_flight1P2);
		panel_flight1P2.setLayout(new GridLayout(4, 4, 0, 0));
		panel_flight1B1 = new JPanel();
		panel_flight1B1.setBounds(214, 36, 92, 98);
		panel_flight1.add(panel_flight1B1);
		panel_flight1B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight1B2 = new JPanel();
		panel_flight1B2.setBounds(316, 36, 92, 98);
		panel_flight1.add(panel_flight1B2);
		panel_flight1B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight1N1 = new JPanel();
		panel_flight1N1.setBounds(418, 36, 92, 98);
		panel_flight1.add(panel_flight1N1);
		panel_flight1N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight1N2 = new JPanel();
		panel_flight1N2.setBounds(520, 36, 92, 98);
		panel_flight1.add(panel_flight1N2);
		panel_flight1N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight2 = new JPanel();
		panel_flight2.setBounds(10, 164, 625, 142);
		panel_6.add(panel_flight2);
		panel_flight2.setLayout(null);
		
		panel_flight2P1 = new JPanel();
		panel_flight2P1.setBounds(10, 33, 92, 98);
		panel_flight2.add(panel_flight2P1);
		panel_flight2P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight2P2 = new JPanel();
		panel_flight2P2.setBounds(112, 33, 92, 98);
		panel_flight2.add(panel_flight2P2);
		panel_flight2P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight2B1 = new JPanel();
		panel_flight2B1.setBounds(214, 33, 92, 98);
		panel_flight2.add(panel_flight2B1);
		panel_flight2B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight2B2 = new JPanel();
		panel_flight2B2.setBounds(316, 33, 92, 98);
		panel_flight2.add(panel_flight2B2);
		panel_flight2B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight2N1 = new JPanel();
		panel_flight2N1.setBounds(418, 33, 92, 98);
		panel_flight2.add(panel_flight2N1);
		panel_flight2N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight2N2 = new JPanel();
		panel_flight2N2.setBounds(520, 33, 92, 98);
		panel_flight2.add(panel_flight2N2);
		panel_flight2N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokchiangmai = new JLabel("Flight2 TS760 Bangkok-Chiangmai");
		lblFlightBangkokchiangmai.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokchiangmai.setBounds(169, 11, 285, 14);
		panel_flight2.add(lblFlightBangkokchiangmai);
		
		panel_flight3 = new JPanel();
		panel_flight3.setBounds(10, 317, 625, 142);
		panel_6.add(panel_flight3);
		panel_flight3.setLayout(null);
		
		panel_flight3P1 = new JPanel();
		panel_flight3P1.setBounds(10, 33, 92, 98);
		panel_flight3.add(panel_flight3P1);
		panel_flight3P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight3P2 = new JPanel();
		panel_flight3P2.setBounds(112, 33, 92, 98);
		panel_flight3.add(panel_flight3P2);
		panel_flight3P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight3B1 = new JPanel();
		panel_flight3B1.setBounds(214, 33, 92, 98);
		panel_flight3.add(panel_flight3B1);
		panel_flight3B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight3B2 = new JPanel();
		panel_flight3B2.setBounds(316, 33, 92, 98);
		panel_flight3.add(panel_flight3B2);
		panel_flight3B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight3N1 = new JPanel();
		panel_flight3N1.setBounds(418, 33, 92, 98);
		panel_flight3.add(panel_flight3N1);
		panel_flight3N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight3N2 = new JPanel();
		panel_flight3N2.setBounds(520, 33, 92, 98);
		panel_flight3.add(panel_flight3N2);
		panel_flight3N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokchiangmai_1 = new JLabel("Flight3 TS700 Bangkok-Chiangmai");
		lblFlightBangkokchiangmai_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokchiangmai_1.setBounds(168, 8, 285, 14);
		panel_flight3.add(lblFlightBangkokchiangmai_1);
		
		panel_flight4 = new JPanel();
		panel_flight4.setBounds(645, 11, 625, 142);
		panel_6.add(panel_flight4);
		panel_flight4.setLayout(null);
		
		panel_flight4P1 = new JPanel();
		panel_flight4P1.setBounds(10, 36, 92, 98);
		panel_flight4.add(panel_flight4P1);
		panel_flight4P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight4P2 = new JPanel();
		panel_flight4P2.setBounds(112, 36, 92, 98);
		panel_flight4.add(panel_flight4P2);
		panel_flight4P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight4B1 = new JPanel();
		panel_flight4B1.setBounds(214, 36, 92, 98);
		panel_flight4.add(panel_flight4B1);
		panel_flight4B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight4B2 = new JPanel();
		panel_flight4B2.setBounds(316, 36, 92, 98);
		panel_flight4.add(panel_flight4B2);
		panel_flight4B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight4N1 = new JPanel();
		panel_flight4N1.setBounds(418, 36, 92, 98);
		panel_flight4.add(panel_flight4N1);
		panel_flight4N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight4N2 = new JPanel();
		panel_flight4N2.setBounds(520, 36, 92, 98);
		panel_flight4.add(panel_flight4N2);
		panel_flight4N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokkampanpetch = new JLabel("Flight4 TS600 Bangkok-Kampanpetch");
		lblFlightBangkokkampanpetch.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokkampanpetch.setBounds(168, 11, 285, 14);
		panel_flight4.add(lblFlightBangkokkampanpetch);
		
		panel_flight5 = new JPanel();
		panel_flight5.setBounds(645, 164, 625, 142);
		panel_6.add(panel_flight5);
		panel_flight5.setLayout(null);
		
		
		
		panel_flight5P1 = new JPanel();
		panel_flight5P1.setBounds(10, 36, 92, 98);
		panel_flight5.add(panel_flight5P1);
		panel_flight5P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight5P2 = new JPanel();
		panel_flight5P2.setBounds(112, 36, 92, 98);
		panel_flight5.add(panel_flight5P2);
		panel_flight5P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight5B1 = new JPanel();
		panel_flight5B1.setBounds(214, 36, 92, 98);
		panel_flight5.add(panel_flight5B1);
		panel_flight5B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight5B2 = new JPanel();
		panel_flight5B2.setBounds(316, 36, 92, 98);
		panel_flight5.add(panel_flight5B2);
		panel_flight5B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight5N1 = new JPanel();
		panel_flight5N1.setBounds(418, 36, 92, 98);
		panel_flight5.add(panel_flight5N1);
		panel_flight5N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight5N2 = new JPanel();
		panel_flight5N2.setBounds(520, 36, 92, 98);
		panel_flight5.add(panel_flight5N2);
		panel_flight5N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokkampanpetch_1 = new JLabel("Flight5 TS610 Bangkok-Kampanpetch");
		lblFlightBangkokkampanpetch_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokkampanpetch_1.setBounds(164, 11, 285, 14);
		panel_flight5.add(lblFlightBangkokkampanpetch_1);
		
		JScrollPane scrollPane = new JScrollPane(panel_6,JScrollPane.VERTICAL_SCROLLBAR_NEVER,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		panel_flight6 = new JPanel();
		panel_flight6.setLayout(null);
		panel_flight6.setBounds(645, 317, 625, 142);
		panel_6.add(panel_flight6);
		
		panel_flight6P1 = new JPanel();
		panel_flight6P1.setBounds(10, 36, 92, 98);
		panel_flight6.add(panel_flight6P1);
		panel_flight6P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight6P2 = new JPanel();
		panel_flight6P2.setBounds(112, 36, 92, 98);
		panel_flight6.add(panel_flight6P2);
		panel_flight6P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight6B1 = new JPanel();
		panel_flight6B1.setBounds(214, 36, 92, 98);
		panel_flight6.add(panel_flight6B1);
		panel_flight6B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight6B2 = new JPanel();
		panel_flight6B2.setBounds(316, 36, 92, 98);
		panel_flight6.add(panel_flight6B2);
		panel_flight6B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight6N1 = new JPanel();
		panel_flight6N1.setBounds(418, 36, 92, 98);
		panel_flight6.add(panel_flight6N1);
		panel_flight6N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight6N2 = new JPanel();
		panel_flight6N2.setBounds(520, 36, 92, 98);
		panel_flight6.add(panel_flight6N2);
		panel_flight6N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokkampanpetch_2 = new JLabel("Flight6 TS620 Bangkok-Kampanpetch");
		lblFlightBangkokkampanpetch_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokkampanpetch_2.setBounds(165, 11, 285, 14);
		panel_flight6.add(lblFlightBangkokkampanpetch_2);
		
		panel_flight7 = new JPanel();
		panel_flight7.setLayout(null);
		panel_flight7.setBounds(1280, 11, 625, 142);
		panel_6.add(panel_flight7);
		
		panel_flight7P1 = new JPanel();
		panel_flight7P1.setBounds(10, 36, 92, 98);
		panel_flight7.add(panel_flight7P1);
		panel_flight7P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight7P2 = new JPanel();
		panel_flight7P2.setBounds(112, 36, 92, 98);
		panel_flight7.add(panel_flight7P2);
		panel_flight7P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight7B1 = new JPanel();
		panel_flight7B1.setBounds(214, 36, 92, 98);
		panel_flight7.add(panel_flight7B1);
		panel_flight7B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight7B2 = new JPanel();
		panel_flight7B2.setBounds(316, 36, 92, 98);
		panel_flight7.add(panel_flight7B2);
		panel_flight7B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight7N1 = new JPanel();
		panel_flight7N1.setBounds(418, 36, 92, 98);
		panel_flight7.add(panel_flight7N1);
		panel_flight7N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight7N2 = new JPanel();
		panel_flight7N2.setBounds(520, 36, 92, 98);
		panel_flight7.add(panel_flight7N2);
		panel_flight7N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokkhonKaen = new JLabel("Flight7 TS710 Bangkok-Khon Kaen");
		lblFlightBangkokkhonKaen.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokkhonKaen.setBounds(168, 11, 285, 14);
		panel_flight7.add(lblFlightBangkokkhonKaen);
		
		panel_flight8 = new JPanel();
		panel_flight8.setLayout(null);
		panel_flight8.setBounds(1280, 164, 625, 142);
		panel_6.add(panel_flight8);
		
		panel_flight8P1 = new JPanel();
		panel_flight8P1.setBounds(10, 36, 92, 98);
		panel_flight8.add(panel_flight8P1);
		panel_flight8P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight8P2 = new JPanel();
		panel_flight8P2.setBounds(112, 36, 92, 98);
		panel_flight8.add(panel_flight8P2);
		panel_flight8P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight8B1 = new JPanel();
		panel_flight8B1.setBounds(214, 36, 92, 98);
		panel_flight8.add(panel_flight8B1);
		panel_flight8B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight8B2 = new JPanel();
		panel_flight8B2.setBounds(316, 36, 92, 98);
		panel_flight8.add(panel_flight8B2);
		panel_flight8B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight8N1 = new JPanel();
		panel_flight8N1.setBounds(418, 36, 92, 98);
		panel_flight8.add(panel_flight8N1);
		panel_flight8N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight8N2 = new JPanel();
		panel_flight8N2.setBounds(520, 36, 92, 98);
		panel_flight8.add(panel_flight8N2);
		panel_flight8N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokkhonKaen_1 = new JLabel("Flight8 TS720 Bangkok-Khon Kaen");
		lblFlightBangkokkhonKaen_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokkhonKaen_1.setBounds(164, 11, 285, 14);
		panel_flight8.add(lblFlightBangkokkhonKaen_1);
		
		panel_flight9 = new JPanel();
		panel_flight9.setLayout(null);
		panel_flight9.setBounds(1280, 317, 625, 142);
		panel_6.add(panel_flight9);
		
		panel_flight9P1 = new JPanel();
		panel_flight9P1.setBounds(10, 36, 92, 98);
		panel_flight9.add(panel_flight9P1);
		panel_flight9P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight9P2 = new JPanel();
		panel_flight9P2.setBounds(112, 36, 92, 98);
		panel_flight9.add(panel_flight9P2);
		panel_flight9P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight9B1 = new JPanel();
		panel_flight9B1.setBounds(214, 36, 92, 98);
		panel_flight9.add(panel_flight9B1);
		panel_flight9B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight9B2 = new JPanel();
		panel_flight9B2.setBounds(316, 36, 92, 98);
		panel_flight9.add(panel_flight9B2);
		panel_flight9B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight9N1 = new JPanel();
		panel_flight9N1.setBounds(418, 36, 92, 98);
		panel_flight9.add(panel_flight9N1);
		panel_flight9N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight9N2 = new JPanel();
		panel_flight9N2.setBounds(520, 36, 92, 98);
		panel_flight9.add(panel_flight9N2);
		panel_flight9N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokkhonKaen_2 = new JLabel("Flight9 TS730 Bangkok-Khon Kaen");
		lblFlightBangkokkhonKaen_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokkhonKaen_2.setBounds(165, 11, 285, 14);
		panel_flight9.add(lblFlightBangkokkhonKaen_2);
		
		panel_flight10 = new JPanel();
		panel_flight10.setLayout(null);
		panel_flight10.setBounds(1915, 11, 625, 142);
		panel_6.add(panel_flight10);
		
		panel_flight10P1 = new JPanel();
		panel_flight10P1.setBounds(10, 36, 92, 98);
		panel_flight10.add(panel_flight10P1);
		panel_flight10P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight10P2 = new JPanel();
		panel_flight10P2.setBounds(112, 36, 92, 98);
		panel_flight10.add(panel_flight10P2);
		panel_flight10P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight10B1 = new JPanel();
		panel_flight10B1.setBounds(214, 36, 92, 98);
		panel_flight10.add(panel_flight10B1);
		panel_flight10B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight10B2 = new JPanel();
		panel_flight10B2.setBounds(316, 36, 92, 98);
		panel_flight10.add(panel_flight10B2);
		panel_flight10B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight10N1 = new JPanel();
		panel_flight10N1.setBounds(418, 36, 92, 98);
		panel_flight10.add(panel_flight10N1);
		panel_flight10N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight10N2 = new JPanel();
		panel_flight10N2.setBounds(520, 36, 92, 98);
		panel_flight10.add(panel_flight10N2);
		panel_flight10N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokhuaHin = new JLabel("Flight10 TS750 Bangkok-Hua Hin");
		lblFlightBangkokhuaHin.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokhuaHin.setBounds(168, 11, 285, 14);
		panel_flight10.add(lblFlightBangkokhuaHin);
		
		panel_flight11 = new JPanel();
		panel_flight11.setLayout(null);
		panel_flight11.setBounds(1915, 164, 625, 142);
		panel_6.add(panel_flight11);
		
		panel_flight11P1 = new JPanel();
		panel_flight11P1.setBounds(10, 36, 92, 98);
		panel_flight11.add(panel_flight11P1);
		panel_flight11P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight11P2 = new JPanel();
		panel_flight11P2.setBounds(112, 36, 92, 98);
		panel_flight11.add(panel_flight11P2);
		panel_flight11P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight11B1 = new JPanel();
		panel_flight11B1.setBounds(214, 36, 92, 98);
		panel_flight11.add(panel_flight11B1);
		panel_flight11B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight11B2 = new JPanel();
		panel_flight11B2.setBounds(316, 36, 92, 98);
		panel_flight11.add(panel_flight11B2);
		panel_flight11B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight11N1 = new JPanel();
		panel_flight11N1.setBounds(418, 36, 92, 98);
		panel_flight11.add(panel_flight11N1);
		panel_flight11N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight11N2 = new JPanel();
		panel_flight11N2.setBounds(520, 36, 92, 98);
		panel_flight11.add(panel_flight11N2);
		panel_flight11N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokhuaHin_1 = new JLabel("Flight11 TS790 Bangkok-Hua Hin");
		lblFlightBangkokhuaHin_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokhuaHin_1.setBounds(164, 11, 285, 14);
		panel_flight11.add(lblFlightBangkokhuaHin_1);
		
		panel_flight12 = new JPanel();
		panel_flight12.setLayout(null);
		panel_flight12.setBounds(1915, 317, 625, 142);
		panel_6.add(panel_flight12);
		
		panel_flight12P1 = new JPanel();
		panel_flight12P1.setBounds(10, 36, 92, 98);
		panel_flight12.add(panel_flight12P1);
		panel_flight12P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight12P2 = new JPanel();
		panel_flight12P2.setBounds(112, 36, 92, 98);
		panel_flight12.add(panel_flight12P2);
		panel_flight12P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight12B1 = new JPanel();
		panel_flight12B1.setBounds(214, 36, 92, 98);
		panel_flight12.add(panel_flight12B1);
		panel_flight12B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight12B2 = new JPanel();
		panel_flight12B2.setBounds(316, 36, 92, 98);
		panel_flight12.add(panel_flight12B2);
		panel_flight12B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight12N1 = new JPanel();
		panel_flight12N1.setBounds(418, 36, 92, 98);
		panel_flight12.add(panel_flight12N1);
		panel_flight12N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight12N2 = new JPanel();
		panel_flight12N2.setBounds(520, 36, 92, 98);
		panel_flight12.add(panel_flight12N2);
		panel_flight12N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokhuaHin_2 = new JLabel("Flight12 TS740 Bangkok-Hua Hin");
		lblFlightBangkokhuaHin_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokhuaHin_2.setBounds(165, 11, 285, 14);
		panel_flight12.add(lblFlightBangkokhuaHin_2);
		
		panel_flight13 = new JPanel();
		panel_flight13.setLayout(null);
		panel_flight13.setBounds(2550, 11, 625, 142);
		panel_6.add(panel_flight13);
		
		panel_flight13P1 = new JPanel();
		panel_flight13P1.setBounds(10, 36, 92, 98);
		panel_flight13.add(panel_flight13P1);
		panel_flight13P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight13P2 = new JPanel();
		panel_flight13P2.setBounds(112, 36, 92, 98);
		panel_flight13.add(panel_flight13P2);
		panel_flight13P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight13B1 = new JPanel();
		panel_flight13B1.setBounds(214, 36, 92, 98);
		panel_flight13.add(panel_flight13B1);
		panel_flight13B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight13B2 = new JPanel();
		panel_flight13B2.setBounds(316, 36, 92, 98);
		panel_flight13.add(panel_flight13B2);
		panel_flight13B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight13N1 = new JPanel();
		panel_flight13N1.setBounds(418, 36, 92, 98);
		panel_flight13.add(panel_flight13N1);
		panel_flight13N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight13N2 = new JPanel();
		panel_flight13N2.setBounds(520, 36, 92, 98);
		panel_flight13.add(panel_flight13N2);
		panel_flight13N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokhatYai = new JLabel("Flight13 TS670 Bangkok-Hat Yai");
		lblFlightBangkokhatYai.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokhatYai.setBounds(168, 11, 285, 14);
		panel_flight13.add(lblFlightBangkokhatYai);
		
		panel_flight14 = new JPanel();
		panel_flight14.setLayout(null);
		panel_flight14.setBounds(2550, 164, 625, 142);
		panel_6.add(panel_flight14);
		
		panel_flight14P1 = new JPanel();
		panel_flight14P1.setBounds(10, 36, 92, 98);
		panel_flight14.add(panel_flight14P1);
		panel_flight14P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight14P2 = new JPanel();
		panel_flight14P2.setBounds(112, 36, 92, 98);
		panel_flight14.add(panel_flight14P2);
		panel_flight14P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight14B1 = new JPanel();
		panel_flight14B1.setBounds(214, 36, 92, 98);
		panel_flight14.add(panel_flight14B1);
		panel_flight14B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight14B2 = new JPanel();
		panel_flight14B2.setBounds(316, 36, 92, 98);
		panel_flight14.add(panel_flight14B2);
		panel_flight14B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight14N1 = new JPanel();
		panel_flight14N1.setBounds(418, 36, 92, 98);
		panel_flight14.add(panel_flight14N1);
		panel_flight14N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight14N2 = new JPanel();
		panel_flight14N2.setBounds(520, 36, 92, 98);
		panel_flight14.add(panel_flight14N2);
		panel_flight14N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokhatYai_1 = new JLabel("Flight14 TS690 Bangkok-Hat Yai");
		lblFlightBangkokhatYai_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokhatYai_1.setBounds(164, 11, 285, 14);
		panel_flight14.add(lblFlightBangkokhatYai_1);
		
		panel_flight15 = new JPanel();
		panel_flight15.setLayout(null);
		panel_flight15.setBounds(2550, 317, 625, 142);
		panel_6.add(panel_flight15);
		
		panel_flight15P1 = new JPanel();
		panel_flight15P1.setBounds(10, 36, 92, 98);
		panel_flight15.add(panel_flight15P1);
		panel_flight15P1.setLayout(new GridLayout(3, 4, 0, 0));
		
		panel_flight15P2 = new JPanel();
		panel_flight15P2.setBounds(112, 36, 92, 98);
		panel_flight15.add(panel_flight15P2);
		panel_flight15P2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight15B1 = new JPanel();
		panel_flight15B1.setBounds(214, 36, 92, 98);
		panel_flight15.add(panel_flight15B1);
		panel_flight15B1.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight15B2 = new JPanel();
		panel_flight15B2.setBounds(316, 36, 92, 98);
		panel_flight15.add(panel_flight15B2);
		panel_flight15B2.setLayout(new GridLayout(4, 4, 0, 0));
		
		panel_flight15N1 = new JPanel();
		panel_flight15N1.setBounds(418, 36, 92, 98);
		panel_flight15.add(panel_flight15N1);
		panel_flight15N1.setLayout(new GridLayout(6, 4, 0, 0));
		
		panel_flight15N2 = new JPanel();
		panel_flight15N2.setBounds(520, 36, 92, 98);
		panel_flight15.add(panel_flight15N2);
		panel_flight15N2.setLayout(new GridLayout(6, 4, 0, 0));
		
		lblFlightBangkokhatYai_2 = new JLabel("Flight15 TS680 Bangkok-Hat Yai");
		lblFlightBangkokhatYai_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightBangkokhatYai_2.setBounds(165, 11, 285, 14);
		panel_flight15.add(lblFlightBangkokhatYai_2);
		scrollPane.setBounds(10, 11, 794, 495);
		frame.getContentPane().add(scrollPane);
		
		initialMonitor(0,panel_flight1P1);
		initialMonitor(1,panel_flight1P2);
		initialMonitor(2,panel_flight1B1);
		initialMonitor(3,panel_flight1B2);
		initialMonitor(4,panel_flight1N1);
		initialMonitor(5,panel_flight1N2);

		initialMonitor(0,panel_flight2P1);
		initialMonitor(1,panel_flight2P2);
		initialMonitor(2,panel_flight2B1);
		initialMonitor(3,panel_flight2B2);
		initialMonitor(4,panel_flight2N1);
		initialMonitor(5,panel_flight2N2);

		initialMonitor(0,panel_flight3P1);
		initialMonitor(1,panel_flight3P2);
		initialMonitor(2,panel_flight3B1);
		initialMonitor(3,panel_flight3B2);
		initialMonitor(4,panel_flight3N1);
		initialMonitor(5,panel_flight3N2);

		initialMonitor(0,panel_flight4P1);
		initialMonitor(1,panel_flight4P2);
		initialMonitor(2,panel_flight4B1);
		initialMonitor(3,panel_flight4B2);
		initialMonitor(4,panel_flight4N1);
		initialMonitor(5,panel_flight4N2);

		initialMonitor(0,panel_flight5P1);
		initialMonitor(1,panel_flight5P2);
		initialMonitor(2,panel_flight5B1);
		initialMonitor(3,panel_flight5B2);
		initialMonitor(4,panel_flight5N1);
		initialMonitor(5,panel_flight5N2);

		initialMonitor(0,panel_flight6P1);
		initialMonitor(1,panel_flight6P2);
		initialMonitor(2,panel_flight6B1);
		initialMonitor(3,panel_flight6B2);
		initialMonitor(4,panel_flight6N1);
		initialMonitor(5,panel_flight6N2);

		initialMonitor(0,panel_flight7P1);
		initialMonitor(1,panel_flight7P2);
		initialMonitor(2,panel_flight7B1);
		initialMonitor(3,panel_flight7B2);
		initialMonitor(4,panel_flight7N1);
		initialMonitor(5,panel_flight7N2);

		initialMonitor(0,panel_flight8P1);
		initialMonitor(1,panel_flight8P2);
		initialMonitor(2,panel_flight8B1);
		initialMonitor(3,panel_flight8B2);
		initialMonitor(4,panel_flight8N1);
		initialMonitor(5,panel_flight8N2);

		initialMonitor(0,panel_flight9P1);
		initialMonitor(1,panel_flight9P2);
		initialMonitor(2,panel_flight9B1);
		initialMonitor(3,panel_flight9B2);
		initialMonitor(4,panel_flight9N1);
		initialMonitor(5,panel_flight9N2);

		initialMonitor(0,panel_flight10P1);
		initialMonitor(1,panel_flight10P2);
		initialMonitor(2,panel_flight10B1);
		initialMonitor(3,panel_flight10B2);
		initialMonitor(4,panel_flight10N1);
		initialMonitor(5,panel_flight10N2);

		initialMonitor(0,panel_flight11P1);
		initialMonitor(1,panel_flight11P2);
		initialMonitor(2,panel_flight11B1);
		initialMonitor(3,panel_flight11B2);
		initialMonitor(4,panel_flight11N1);
		initialMonitor(5,panel_flight11N2);

		initialMonitor(0,panel_flight12P1);
		initialMonitor(1,panel_flight12P2);
		initialMonitor(2,panel_flight12B1);
		initialMonitor(3,panel_flight12B2);
		initialMonitor(4,panel_flight12N1);
		initialMonitor(5,panel_flight12N2);

		initialMonitor(0,panel_flight13P1);
		initialMonitor(1,panel_flight13P2);
		initialMonitor(2,panel_flight13B1);
		initialMonitor(3,panel_flight13B2);
		initialMonitor(4,panel_flight13N1);
		initialMonitor(5,panel_flight13N2);

		initialMonitor(0,panel_flight14P1);
		initialMonitor(1,panel_flight14P2);
		initialMonitor(2,panel_flight14B1);
		initialMonitor(3,panel_flight14B2);
		initialMonitor(4,panel_flight14N1);
		initialMonitor(5,panel_flight14N2);

		initialMonitor(0,panel_flight15P1);
		initialMonitor(1,panel_flight15P2);
		initialMonitor(2,panel_flight15B1);
		initialMonitor(3,panel_flight15B2);
		initialMonitor(4,panel_flight15N1);
		initialMonitor(5,panel_flight15N2);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(814, 11, 175, 495);
		frame.getContentPane().add(scrollPane_1);

		model = new DefaultTableModel();
		table = new JTable(model);
		model.addColumn("Flight");
		model.addColumn("Seat NO");
		
		scrollPane_1.setViewportView(table);
		Database.getAllSeatLayoutSQL();
		realTime();
	}
	
	public void realTime(){
		Runnable run = new Runnable(){
			public void run(){
				while(true){
					Database.getAllSeatLayoutSQL();
					new realTimeClass(0).thread();
					new realTimeClass(1).thread();
					new realTimeClass(2).thread();
					new realTimeClass(3).thread();
					new realTimeClass(4).thread();
					new realTimeClass(5).thread();
					new realTimeClass(6).thread();
					new realTimeClass(7).thread();
					new realTimeClass(8).thread();
					new realTimeClass(9).thread();
					new realTimeClass(10).thread();
					new realTimeClass(11).thread();
					new realTimeClass(12).thread();
					new realTimeClass(13).thread();
					new realTimeClass(14).thread();
					
				}
				
			}
		};
		Thread thread  = new Thread(run);
		thread.start();
	}
	public void updateTable(int flight,String seatLetter,int i){
		if(bookedList.size()==0){
			bookedList.add(new String[]{(flight)+"",seatLetter + (i+1)});
			model.addRow(new Object[]{flight,seatLetter + (i+1)});
		}else{
			for(int j=0;j<bookedList.size();j++){
				if(flight==Integer.parseInt(bookedList.get(j)[0]) && (seatLetter+(i+1)).equals(bookedList.get(j)[1])){
					break;
				}else if(j==bookedList.size()-1){
					bookedList.add(new String[]{(flight)+"",seatLetter + (i+1)});
					model.addRow(new Object[]{flight,seatLetter + (i+1)});
				}
			}
		}
		
	
	}
	public void deleteRow(int flight,String seatLetter,int i){
		for(int j=0;j<bookedList.size();j++){
			if(flight==Integer.parseInt(bookedList.get(j)[0]) && (seatLetter+(i+1)).equals(bookedList.get(j)[1])){
				model.removeRow(j);
				bookedList.remove(j);
				break;
			}
		}
	}
	public class realTimeClass{
		private int flight;
		public realTimeClass(int flight){
			this.flight = flight;
		}
		public void thread(){
				for(int i=0;i<28;i++){
							if(Database.getAllSeat().get(((flight+1)*3)-3)[i].equals("1")){
								MonitorController.labelArr.get((flight*108)+i).setBackground(Color.RED);
								updateTable(flight+1,"P",i);
							}else{
								MonitorController.labelArr.get((flight*108)+i).setBackground(Color.GREEN);
								deleteRow(flight+1,"P",i);
							}
						}
						for(int i=0;i<32;i++){
							if(Database.getAllSeat().get(((flight+1)*3)-2)[i].equals("1")){
								MonitorController.labelArr.get(((flight*108)+28)+i).setBackground(Color.RED);
								updateTable(flight+1,"B",i);
							}else{
								MonitorController.labelArr.get(((flight*108)+28)+i).setBackground(Color.GREEN);
								deleteRow(flight+1,"B",i);
							}
						}
						for(int i=0;i<48;i++){
							if(Database.getAllSeat().get(((flight+1)*3)-1)[i].equals("1")){
								MonitorController.labelArr.get(((flight*108)+60)+i).setBackground(Color.RED);
								updateTable(flight+1,"N",i);
							}else{
								MonitorController.labelArr.get(((flight*108)+60)+i).setBackground(Color.GREEN);
								deleteRow(flight+1,"N",i);
							}
						}
					}
			
		}
	
	
//	public void realTime(final int flight){
//		Runnable run = new Runnable(){
//			public void run(){
//				while(true){
//					Database.getAllSeatLayoutSQL();
//						for(int i=0;i<28;i++){
//							if(Database.allSeat.get(((flight+1)*3)-3)[i].equals("1")){
//								MonitorController.labelArr.get((flight*108)+i).setBackground(Color.RED);
//							}else{
//								MonitorController.labelArr.get((flight*108)+i).setBackground(Color.GREEN);
//							}
//						}
//						for(int i=0;i<32;i++){
//							System.out.println("FLIGHT : " + flight);
//							System.out.println("Index : " + (((flight+1)*3)-2));
//							if(Database.allSeat.get(((flight+1)*3)-2)[i].equals("1")){
//								MonitorController.labelArr.get(((flight*108)+28)+i).setBackground(Color.RED);
//							}else{
//								MonitorController.labelArr.get(((flight*108)+28)+i).setBackground(Color.GREEN);
//							}
//						}
//						for(int i=0;i<48;i++){
//							if(Database.allSeat.get(((flight+1)*3)-1)[i].equals("1")){
//								MonitorController.labelArr.get(((flight*108)+60)+i).setBackground(Color.RED);
//							}else{
//								MonitorController.labelArr.get(((flight*108)+60)+i).setBackground(Color.GREEN);
//							}
//						}
//					}
//					
//				}
//			
//		};
//		new Thread(run).start();
//	}
	public void initialMonitor(int option,JPanel panel){
		//option=0 premium1
		//option=1 premium2
		//option=2 business1
		//option=3 business2
		//option=4 normal1
		//option=5 normal2

		int count=1;
		if(option==0){
			for(int i=0;i<3;i++){
				for(int k=0;k<4;k++){
					labelArr.add(new JLabel(count+""));
					labelArr.get(labelArr.size()-1).setOpaque(true);
					panel.add(labelArr.get(labelArr.size()-1));
					count++;
				}
			}
		}else if(option==1){
			count=13;
			for(int i=0;i<4;i++){
				for(int k=0;k<4;k++){
					labelArr.add(new JLabel(count+""));
					labelArr.get(labelArr.size()-1).setOpaque(true);
					panel.add(labelArr.get(labelArr.size()-1));
					count++;
				}
			}
		}else if(option==2){
			for(int i=0;i<4;i++){
				for(int k=0;k<4;k++){
					labelArr.add(new JLabel(count+""));
					labelArr.get(labelArr.size()-1).setOpaque(true);
					panel.add(labelArr.get(labelArr.size()-1));
					count++;
				}
			}
		}else if(option==3){
			count=17;
			for(int i=0;i<4;i++){
				for(int k=0;k<4;k++){
					labelArr.add(new JLabel(count+""));
					labelArr.get(labelArr.size()-1).setOpaque(true);
					panel.add(labelArr.get(labelArr.size()-1));
					count++;
				}
			}
		}else if(option==4){
			for(int i=0;i<6;i++){
				for(int k=0;k<4;k++){
					labelArr.add(new JLabel(count+""));
					labelArr.get(labelArr.size()-1).setOpaque(true);
					panel.add(labelArr.get(labelArr.size()-1));
					count++;
				}
			}
		}else if(option==5){
			count=25;
			for(int i=0;i<6;i++){
				for(int k=0;k<4;k++){
					labelArr.add(new JLabel(count+""));
					labelArr.get(labelArr.size()-1).setOpaque(true);
					panel.add(labelArr.get(labelArr.size()-1));
					count++;
				}
			}
		}
		
	}
}
	

