package GUI;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import SourceCode.CreateFlight;
import SourceCode.Customer;
import SourceCode.Database;
import SourceCode.Flight;
import SourceCode.Person;


public class Search {

	protected JFrame frame;
	private JTable table;
	private JTextField txt_firstname;
	private JTextField txt_lastname;
	private JScrollPane scrollPane;
	private JButton btnBack;
	private JRadioButton rdbtn_ticket;
	private JRadioButton rdbtn_customer;
	private JLabel lblNewLabel_1;
	private JRadioButton rdbtnFlight;
	private JPanel panel_general;
	private JPanel panel_flight;
	private JTextField txt_flightinfo;
	private JLabel lblFlightinfo;
	private JPopupMenu popup;
	private JMenuItem menuItem_cancelFlight,menuItem_flight,menuItem_ticketList,menuItem_billList;
	private Flight flight;
	private int option;
	private int flightIndex;
	/**
	 * Create the application.
	 */
	public Search(int option) {
		this.option = option;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Search");
		frame.setResizable(false);
		frame.setBounds(100, 100, 1041, 225);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		popup = new JPopupMenu();
		menuItem_cancelFlight = new JMenuItem("Cancel");
		menuItem_cancelFlight.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				equalsForCancelFlight();
			}
		});
		
		menuItem_flight = new JMenuItem("View passenger list");
		menuItem_flight.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showPassengerList();
			}
		});
		menuItem_ticketList = new JMenuItem("View ticket list");
		menuItem_ticketList.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showTicketList();
			}
		});
		
		menuItem_billList = new JMenuItem("View bill list");
		menuItem_billList.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				showBillList();
			}
		});
		table = new JTable();
		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(309, 6, 722, 187);
		frame.getContentPane().add(scrollPane);
		
		JButton btnNewButton = new JButton("Search");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdbtn_ticket.isSelected()){
					searchTicket();
					popup.removeAll();
					popup.add(menuItem_cancelFlight);
					table.setComponentPopupMenu(popup);
				}else if(rdbtn_customer.isSelected()){
					for(int i=0;i<Database.getLoginArr().size();i++){
						if(Database.getLoginArr().get(i)[3].equalsIgnoreCase(txt_firstname.getText()) && Database.getLoginArr().get(i)[4].equalsIgnoreCase(txt_lastname.getText())){
							String[] tableHeader = {"Customer ID","Firstname","Lastname","Telephone","Email","Gender","Member class"};
							String[][] customerInfo = new String[1][7];
							
							int countK=0;
							for(int k =0;k<Database.getLoginArr().get(i).length;k++){
								if(k!=1 && k!=2){
									customerInfo[0][countK] =Database.getLoginArr().get(i)[k];
									countK++;
								}
							}
							table.setModel(new DefaultTableModel(customerInfo,tableHeader));
							popup.removeAll();
							popup.add(menuItem_ticketList);
							popup.add(menuItem_billList);
							table.setComponentPopupMenu(popup);
							break;
						}else if(i==Database.getLoginArr().size()-1){
							JOptionPane.showMessageDialog(null, "Customer not found");
						}
					}
				}else if(rdbtnFlight.isSelected()){
						String[][] flightInfo = new String[1][4];
						boolean found=false;
					for(int i =0;i<CreateFlight.flight.length;i++){
						if(CreateFlight.flight[i].getFlightInfo().getInfo().equalsIgnoreCase(txt_flightinfo.getText())){
							flight = CreateFlight.flight[i];
							flightIndex = i;
							flightInfo[0][0] = CreateFlight.flight[i].getFlightInfo().getInfo();
							flightInfo[0][1] = CreateFlight.flight[i].getFlightInfo().getFromCity();
							flightInfo[0][2] = CreateFlight.flight[i].getFlightInfo().getToCity();
							flightInfo[0][3] = CreateFlight.flight[i].getFlightInfo().getFromtime() + " - " + CreateFlight.flight[i].getFlightInfo().getTotime();
							found=true;
						}else if(i==CreateFlight.flight.length-1 && !found){
							JOptionPane.showMessageDialog(null, "Flight not found");
						}
					}
					if(found){
					String[] tableHeader = {"Flight","From","To","Time"};
					table.setModel(new DefaultTableModel(flightInfo,tableHeader));
					popup.removeAll();
					popup.add(menuItem_flight);
					table.setComponentPopupMenu(popup);
					}
				}
			}
		});
		
		panel_flight = new JPanel();
		panel_flight.setLayout(null);
		panel_flight.setBounds(32, 16, 246, 72);
		panel_flight.setVisible(false);
		
		panel_general = new JPanel();
		panel_general.setBounds(32, 16, 246, 72);
		frame.getContentPane().add(panel_general);
		panel_general.setLayout(null);
		
		txt_lastname = new JTextField();
		txt_lastname.setBounds(92, 42, 140, 20);
		panel_general.add(txt_lastname);
		txt_lastname.setColumns(10);
		
		
		
		txt_firstname = new JTextField();
		txt_firstname.setBounds(92, 11, 140, 20);
		panel_general.add(txt_firstname);
		txt_firstname.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Firstname");
		lblNewLabel.setBounds(0, 14, 82, 14);
		panel_general.add(lblNewLabel);
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel lblLastname = new JLabel("Lastname");
		lblLastname.setBounds(0, 45, 82, 14);
		panel_general.add(lblLastname);
		lblLastname.setHorizontalAlignment(SwingConstants.RIGHT);
		frame.getContentPane().add(panel_flight);
		
		txt_flightinfo = new JTextField();
		txt_flightinfo.setColumns(10);
		txt_flightinfo.setBounds(92, 26, 140, 20);
		panel_flight.add(txt_flightinfo);
		
		lblFlightinfo = new JLabel("FlightInfo");
		lblFlightinfo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFlightinfo.setBounds(0, 29, 82, 14);
		panel_flight.add(lblFlightinfo);
		btnNewButton.setBounds(39, 170, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(140, 170, 89, 23);
		frame.getContentPane().add(btnBack);
		
		rdbtn_ticket = new JRadioButton("Ticket");
		rdbtn_ticket.setBounds(10, 139, 80, 23);
		rdbtn_ticket.setSelected(true);
		rdbtn_ticket.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				rdbtn_customer.setSelected(false);
				rdbtnFlight.setSelected(false);
				panel_general.setVisible(true);
				panel_flight.setVisible(false);
			}
		});
		frame.getContentPane().add(rdbtn_ticket);
		
		rdbtn_customer = new JRadioButton("Customer");
		rdbtn_customer.setBounds(94, 139, 106, 23);
		rdbtn_customer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				rdbtn_ticket.setSelected(false);
				rdbtnFlight.setSelected(false);
				panel_general.setVisible(true);
				panel_flight.setVisible(false);
			}
		});
		frame.getContentPane().add(rdbtn_customer);
		
		lblNewLabel_1 = new JLabel("Search?");
		lblNewLabel_1.setBounds(20, 110, 180, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		rdbtnFlight = new JRadioButton("Flight");
		rdbtnFlight.setBounds(199, 139, 98, 23);
		rdbtnFlight.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(rdbtnFlight.isSelected()){
					rdbtn_ticket.setSelected(false);
					rdbtn_customer.setSelected(false);
					panel_general.setVisible(false);
					panel_flight.setVisible(true);
				}
			}
		});
		if(option==1){
			rdbtnFlight.setVisible(false);
			rdbtn_customer.setVisible(false);
		}
		frame.getContentPane().add(rdbtnFlight);
		
	}
	public void searchTicket(){
		ArrayList<String[]> informationList = new ArrayList<String[]>();
		Customer passenger=null;
		for(int i=0;i<Person.getCustomerList().size();i++){
			if(txt_firstname.getText().equalsIgnoreCase(Person.getCustomerList().get(i).getName()) &&
					txt_lastname.getText().equalsIgnoreCase(Person.getCustomerList().get(i).getLastname())){
				passenger = Person.getCustomerList().get(i);
			}
		}
		for(int i=0;i<passenger.getTicketList().size();i++){
			String ticketInfo = passenger.getName() + "," + passenger.getLastname() + "," + passenger.getTicketList().get(i).toString();
			informationList.add(ticketInfo.split(","));
		}
		String[][] informationPerPerson = new String[informationList.size()][10];
		for(int i =0;i<informationPerPerson.length;i++){
			informationPerPerson[i] = informationList.get(i);
		}
		String[] tableHeader = {"Firstname","Lastname","Flight","From","To","Time","Travel Class","Seat","Price","Status"};
		table.setModel(new DefaultTableModel(informationPerPerson,tableHeader));
	}
	public void equalsForCancelFlight(){
		//get customer for get ticketList
		Customer customer=null;
		for(int i=0;i<Person.getCustomerList().size();i++){
			String name  = (String)table.getValueAt(table.getSelectedRow(), 0);
			String lastname = (String)table.getValueAt(table.getSelectedRow(), 1);
			if(Person.getCustomerList().get(i).equals(name, lastname)){
				customer = Person.getCustomerList().get(i);
			}
		}
		//remove ticket from  ticketlist from customer
		for(int i=0;i<Database.getTicketArr().size();i++){
			String firstname = Database.getTicketArr().get(i)[0];
			String lastname = Database.getTicketArr().get(i)[1];
			String flightInfo = Database.getTicketArr().get(i)[2];
			String seatNO = Database.getTicketArr().get(i)[7];
			if(firstname.equals((String)table.getValueAt(table.getSelectedRow(),0)) &&
					lastname.equals((String)table.getValueAt(table.getSelectedRow(),1)) &&
					flightInfo.equals((String)table.getValueAt(table.getSelectedRow(),2)) &&
					seatNO.equals((String)table.getValueAt(table.getSelectedRow(), 7))){
				cancelSeatLayout(flightInfo,seatNO);
				Database.getTicketArr().remove(i);
				customer.removeSpecificTicket((String)table.getValueAt(table.getSelectedRow(),2), (String)table.getValueAt(table.getSelectedRow(), 7));
				Database.saveTicketList();
				searchTicket();
				break;
			}
		}
	}
	public void showPassengerList(){
		ShowPassengerList showpassenger_main = new ShowPassengerList(flight,flightIndex);
		showpassenger_main.frame.setVisible(true);
	}
	public void showTicketList(){
		String firstname = (String)table.getValueAt(0, 1);
		String lastname = (String)table.getValueAt(0, 2);
		for(int i=0;i<Person.getCustomerList().size();i++){
			if(Person.getCustomerList().get(i).equals(firstname, lastname)){
				ShowTicketList showticketlist_main = new ShowTicketList(Person.getCustomerList().get(i),2);
				showticketlist_main.frame.setVisible(true);
				break;
			}
		}
	}
	public void showBillList(){
		String firstname = (String)table.getValueAt(0, 1);
		String lastname = (String)table.getValueAt(0, 2);
		Customer customer=null;
		for(int i=0;i<Person.getCustomerList().size();i++){
			if(Person.getCustomerList().get(i).equals(firstname,lastname)){
				customer = Person.getCustomerList().get(i);
			}
		}
		ShowBillList billList_main = new ShowBillList(customer);
		billList_main.frame.setVisible(true);
	}
	public void cancelSeatLayout(String flightInfo,String seatNO){
		Flight flight=null;
		int seatLayoutIndex=0;
		int seatIndex=0;
		String[] seatLayout;
		for(int i=0;i<CreateFlight.flight.length;i++){
			if(CreateFlight.flight[i].getFlightInfo().getInfo().equals(flightInfo)){
				seatIndex = Integer.parseInt(seatNO.substring(1))-1;
				Database.flightIndex = i+1;// for declare flightIndex in database
				Database.getSeatLayoutSQL(); // set seatLayout from sql
				if(seatNO.startsWith("P")){
					seatLayoutIndex = ((i+1)*3)-3;
					seatLayout = Database.getSeatLayoutRefPremium();
				}else if(seatNO.startsWith("B")){
					seatLayoutIndex = ((i+1)*3)-2;
					seatLayout = Database.getSeatLayoutRefBusiness();
				}else{
					seatLayoutIndex = ((i+1)*3)-1;
					seatLayout = Database.getSeatLayoutRefNormal();
				}
				seatLayout[seatIndex] = "0";
				Database.saveSeatLayoutSQL();
			}
		}
	}
}
