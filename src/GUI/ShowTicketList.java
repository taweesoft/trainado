package GUI;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import SourceCode.CreateFlight;
import SourceCode.Customer;
import SourceCode.Flight;
import SourceCode.Store;


public class ShowTicketList {

	protected JFrame frame;
	private Customer passenger;
	private JTable table;
	String[] tableHeader = {"Flight","From","To","Time","Travel class","Seat NO","Price","Status"};
	private int option;
	
	public ShowTicketList(Customer passenger,int option) {
		this.option = option;
		this.passenger = passenger;
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle(passenger.getName() + " " + passenger.getLastname() + " : Ticket list");
		frame.setBounds(100, 100, 820, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		
		frame.getContentPane().setLayout(null);
		JPopupMenu popup = new JPopupMenu();
		JMenuItem menuItem_paid = new JMenuItem("Pay");
		menuItem_paid.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(!table.getValueAt(table.getSelectedRow(),7).equals("Paid")){
				String flightInfo = passenger.getTicketList().get(table.getSelectedRow()).getFlightInfo().getInfo();
				Flight flight=null;
				String seatNO = passenger.getTicketList().get(table.getSelectedRow()).getSeatNO();
				for(int i=0;i<CreateFlight.flight.length;i++){
					if(CreateFlight.flight[i].getFlightInfo().getInfo().equals(flightInfo)){
						flight=CreateFlight.flight[i];
					}
				}
				Store.receivePayment(flight, passenger,seatNO);
				table.setModel(new DefaultTableModel(getList(),tableHeader));
				AdminStore.updateFinance();
				}
			}
		});
		popup.add(menuItem_paid);
		
		
		table = new JTable(getList(),tableHeader);
		if(option==1){
			table.setComponentPopupMenu(popup);
		}
		JScrollPane scrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 781, 239);
		frame.getContentPane().add(scrollPane);
	}
	public String[][] getList(){
		String[][] ticketArr = new String[passenger.getTicketList().size()][8];
		for(int i=0;i<ticketArr.length;i++){
			ticketArr[i] = passenger.getTicketList().get(i).toString().split(",");
		}
		return ticketArr;
		
	}

}
