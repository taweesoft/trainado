package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import SourceCode.Store;


public class AdminStore {

	protected JFrame frame;
	ClassLoader loader = this.getClass().getClassLoader();
	private JLabel label_search;
	private JLabel label_recent;
	private JLabel label_customer;
	private static JLabel label_revenue;
	private static JLabel label_profit;
	private static JLabel label_expense;
	private JLabel label_monitor;
	public AdminStore() {
		initialize();
		updateFinance();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Administrator");
		frame.setResizable(false);
		frame.setBounds(100, 100, 576, 404);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		
		label_search = new JLabel("");
		label_search.setBounds(10, 155, 95, 86);
		label_search.addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseReleased(MouseEvent e){
				Search search_main = new Search(2);
				search_main.frame.setVisible(true);
			}
		});
		frame.getContentPane().add(label_search);
		
		label_recent = new JLabel("");
		label_recent.setBounds(145, 155, 95, 76);
		label_recent.addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseReleased(MouseEvent e){
				ShowRecent showrecent_main = new ShowRecent();
				showrecent_main.frame.setVisible(true);
			}
		});
		frame.getContentPane().add(label_recent);
		
		label_customer = new JLabel("");
		label_customer.setBounds(302, 155, 95, 76);
		label_customer.addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseReleased(MouseEvent e){
				ShowCustomerList showcustomerlist_main = new ShowCustomerList();
				showcustomerlist_main.frame.setVisible(true);
			}
		});
		frame.getContentPane().add(label_customer);
		
		label_revenue = new JLabel("Revenue : ");
		label_revenue.setHorizontalAlignment(SwingConstants.LEFT);
		label_revenue.setForeground(Color.WHITE);
		label_revenue.setBounds(10, 111, 150, 14);
		frame.getContentPane().add(label_revenue);
		
		label_monitor = new JLabel("");
		label_monitor.addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseReleased(MouseEvent e){
				MonitorController.frame.setVisible(true);
			}
		});
		label_monitor.setBounds(446, 143, 95, 98);
		frame.getContentPane().add(label_monitor);
		
		label_profit = new JLabel("Profit : ");
		label_profit.setHorizontalAlignment(SwingConstants.CENTER);
		label_profit.setForeground(Color.WHITE);
		label_profit.setBounds(210, 111, 150, 14);
		frame.getContentPane().add(label_profit);
		
		label_expense = new JLabel("Expense : ");
		label_expense.setHorizontalAlignment(SwingConstants.RIGHT);
		label_expense.setForeground(Color.WHITE);
		label_expense.setBounds(416, 111, 150, 14);
		frame.getContentPane().add(label_expense);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				Main.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		lblNewLabel_1.setBounds(210, 294, 187, 67);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon(loader.getResource("images/admin_main.gif")));
		lblNewLabel.setBounds(0, 0, 576, 383);
		frame.getContentPane().add(lblNewLabel);
	}
	public static void updateFinance(){
		label_revenue.setText(String.format("Revenue : %.0f",Store.getRevenue()));
		label_profit.setText(String.format("Profit : %.0f",(Store.getRevenue() - Store.getExpense())));
		label_expense.setText(String.format("Expense : %.0f",Store.getExpense()));
	}
}
