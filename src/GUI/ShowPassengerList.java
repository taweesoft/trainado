package GUI;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import SourceCode.Customer;
import SourceCode.Database;
import SourceCode.Flight;


public class ShowPassengerList {

	protected JFrame frame;
	private JTable table;
	private Flight flight;
	private JPopupMenu popup;
	private JMenuItem menuitem;
	private String[] tableHeader;
	private int flightIndex;
	public ShowPassengerList(Flight flight,int flightIndex) {
		this.flight = flight;
		this.flightIndex = flightIndex;
		initialize();
		frame.setResizable(false);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle(flight.getFlightInfo().getInfo() + " : Passenger list");
		frame.setBounds(100, 100, 820, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		
		
		tableHeader  = new String[]{"Customer ID","Firstname","Lastname","Telephone","Email","Gender","Class","Seat NO"};
	
		table = new JTable(getList(),tableHeader);
		JScrollPane scrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 781, 239);
		frame.getContentPane().add(scrollPane);
		
		popup = new JPopupMenu();
		menuitem = new JMenuItem("Clear flight");
		menuitem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Database.flightIndex = flightIndex+1;
				Database.getSeatLayoutSQL();
				flight.clear();
				Database.saveTicketList();
				table.setModel(new DefaultTableModel(getList(),tableHeader));
			}
		});
		table.setComponentPopupMenu(popup);
		popup.add(menuitem);
		
		
	}
	
	public String[][] getList(){
		ArrayList<String[]> passengerList = new ArrayList<String[]>();
		ArrayList<Customer> newPassengerList = new ArrayList<Customer>();
		for(int i=0;i<flight.getPassengerList().size();i++){ //check for same passenger because one passenger can have more than one ticket
			if(i==0)
				newPassengerList.add(flight.getPassengerList().get(i));
			for(int k=0;k<newPassengerList.size();k++){
				if(!newPassengerList.get(k).equals(flight.getPassengerList().get(i))){
					newPassengerList.add(flight.getPassengerList().get(i));
				}
			}
		}
		for(int i=0;i<newPassengerList.size();i++){
			for(int k=0;k<newPassengerList.get(i).getTicketList().size();k++){
				if(newPassengerList.get(i).getTicketList().get(k).getFlightInfo().getInfo().equals(flight.getFlightInfo().getInfo())){
					String passengerSeat = newPassengerList.get(i).getTicketList().get(k).getSeatNO();
					passengerList.add((newPassengerList.get(i).toString()+","+passengerSeat).split(","));
				}
				
			}
			
		}
		String[][] passengerArr = new String[passengerList.size()][8];
		for(int i=0;i<passengerArr.length;i++){
			passengerArr[i] = passengerList.get(i);
		}
		return passengerArr;
	}
}
