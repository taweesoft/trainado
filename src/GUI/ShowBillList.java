package GUI;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import SourceCode.Customer;


public class ShowBillList {

	protected JFrame frame;
	private Customer customer;
	private JTable table;
	private String[] tableHeader = {"Bill ID","Firstname","Lastname","Flight","Seat NO","Price"};
	public ShowBillList(Customer customer) {
		this.customer = customer;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Bill List");
		frame.setBounds(100, 100, 626, 288);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2- frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		
		
		table = new JTable(getBillList(),tableHeader);
		JScrollPane scrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 601, 239);
		frame.getContentPane().add(scrollPane);
	}
	public String[][] getBillList(){
		String[][] billArr = new String[customer.getBillList().size()][4];
		for(int i=0;i<customer.getBillList().size();i++){
			billArr[i] = customer.getBillList().get(i).toString().split(",");
		}
		return billArr;
	}
}
