package GUI;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import SourceCode.Customer;
import SourceCode.Database;
import SourceCode.Flight;


public class ShowTicket {

	protected JFrame frame;
	private Customer customer;
	private Flight flight;
	private ArrayList<String> bookedNO;
	private int count=0;
	private double totalPrice=0;
	private int confirmedSeat=0;
	/**
	 * Launch the application.
	 */
	private JLabel label_seatType;
	private JLabel label_seatNO;
	private JLabel label_price;
	ClassLoader loader = this.getClass().getClassLoader();
	private JLabel label_confirm;

	/**
	 * Create the application.
	 */
	public ShowTicket(Customer customer,Flight flight,ArrayList<String> bookedNO) {
		this.customer = customer;
		this.flight = flight;
		this.bookedNO = bookedNO;
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Ticket");
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		frame.setBounds(0, 0, 1000, 398);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2 - frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1001, 409);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		label_confirm = new JLabel(new ImageIcon(loader.getResource("images/confirm1.png")));
		label_confirm.setBounds(752, 319, 217, 56);
		label_confirm.addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				int answer =JOptionPane.showConfirmDialog(null, "Confirm booking","Confirm booking",JOptionPane.YES_NO_OPTION);
				if(answer==JOptionPane.YES_OPTION){ 
						confirmedSeat++;
						flight.booking(customer, checkSeatType(),getSeatNO());
						totalPrice += flight.getPrice_PreBooking(checkSeatType(),customer.getMemberClass());
						if(count==bookedNO.size()-1){
							Payment payment_main = new Payment(totalPrice,customer,flight,confirmedSeat,bookedNO);
							payment_main.frame.setVisible(true);
							frame.setVisible(false);
						}
						showNextTicket();
						Database.saveSeatLayoutSQL();
					
					
				}else if(answer==JOptionPane.NO_OPTION){ //no but not last seat
					if(count==bookedNO.size()-1){
						if(confirmedSeat==0){
							Main.frame.setVisible(true);
							frame.setVisible(false);
						}else{
							Payment payment_main = new Payment(totalPrice,customer,flight,confirmedSeat,bookedNO);
							payment_main.frame.setVisible(true);
							frame.setVisible(false);
						}
						
					}
					cancelBookedSeat();
					showNextTicket();
						Database.saveSeatLayoutSQL();
				}
			}
			public void mouseClicked(MouseEvent e){}
			public void mouseEntered(MouseEvent e){
				label_confirm.setIcon(new ImageIcon(loader.getResource("images/confirm2.png")));
			}
			public void mouseExited(MouseEvent e){
				label_confirm.setIcon(new ImageIcon(loader.getResource("images/confirm1.png")));
			}
			public void mousePressed(MouseEvent e){}
		});
		panel.add(label_confirm);
		
		JLabel lblFlight = new JLabel("Flight : " + flight.getFlightInfo().getInfo());
		lblFlight.setFont(new Font("Courier New", Font.BOLD, 20));
		lblFlight.setBounds(67, 132, 309, 25);
		panel.add(lblFlight);
		
		JLabel label_from = new JLabel("From : " + flight.getFlightInfo().getFromCity());
		label_from.setFont(new Font("Courier New", Font.BOLD, 20));
		label_from.setBounds(67, 168, 309, 25);
		panel.add(label_from);
		
		JLabel label_to = new JLabel("To : " + flight.getFlightInfo().getToCity());
		label_to.setFont(new Font("Courier New", Font.BOLD, 20));
		label_to.setBounds(386, 168, 256, 25);
		panel.add(label_to);
		
		JLabel lblFirstname = new JLabel("Name : " + customer.getName() + "." + Character.toString(customer.getLastname().charAt(0)).toUpperCase());
		lblFirstname.setFont(new Font("Courier New", Font.BOLD, 20));
		lblFirstname.setBounds(67, 204, 309, 27);
		panel.add(lblFirstname);
		
		label_seatType = new JLabel("Seat type : " + checkSeatType());
		label_seatType.setFont(new Font("Courier New", Font.BOLD, 20));
		label_seatType.setBounds(67, 242, 309, 25);
		panel.add(label_seatType);
		
		label_seatNO = new JLabel("Seat NO : " + getSeatNO());
		label_seatNO.setFont(new Font("Courier New", Font.BOLD, 20));
		label_seatNO.setBounds(386, 204, 275, 27);
		panel.add(label_seatNO);
		
		
		label_price = new JLabel(flight.getPrice_PreBooking(checkSeatType(),customer.getMemberClass())+ " Baht");
		label_price.setFont(new Font("Courier New", Font.BOLD, 20));
		label_price.setBounds(386, 319, 178, 25);
		panel.add(label_price);
		
		JLabel label_memberClass = new JLabel();
		label_memberClass.setFont(new Font("Courier New", Font.BOLD, 20));
		if(customer.getMemberClass()==0)
			label_memberClass.setText("Member class : non-member");
		else if(customer.getMemberClass()==1)
			label_memberClass.setText("Member class : " + customer.getMemberClass() + " (Discount 10%)");
		else 
			label_memberClass.setText("Member class : " + customer.getMemberClass() + " (Discount 20%");
		
		label_memberClass.setBounds(67, 278, 497, 25);
		panel.add(label_memberClass);

		ClassLoader loader = this.getClass().getClassLoader();
		JLabel lblNewLabel = new JLabel(new ImageIcon(loader.getResource("images/ticket.png")));
		lblNewLabel.setBounds(0, 0, 1000, 398);
		panel.add(lblNewLabel);
	
	}
	public String checkSeatType(){
		if(bookedNO.get(count).startsWith("P"))
			return "Premium";
		else if(bookedNO.get(count).startsWith("B"))
			return "Business";
		else if(bookedNO.get(count).startsWith("N"))
			return "Normal";
		return "";
	}
	public String getSeatNO(){
		return bookedNO.get(count);
	}
	public void cancelBookedSeat(){
		String[] seatLayout;
		if(checkSeatType().equals("Premium")){
			seatLayout = Database.getSeatLayoutRefPremium();
		}else if(checkSeatType().equals("Business")){
			seatLayout = Database.getSeatLayoutRefBusiness();
		}else{
			seatLayout = Database.getSeatLayoutRefNormal();
		}
		int seatIndex = Integer.parseInt(getSeatNO().substring(1, getSeatNO().length()))-1;
		seatLayout[seatIndex] = "0";
		bookedNO.remove(count);
		count--;
	}
	public void showNextTicket(){
		count++;
		if(count<bookedNO.size()){
		label_seatType.setText("Seat type : " + checkSeatType());
		label_seatNO.setText("Seat NO : " + getSeatNO());
		label_price.setText(flight.getPrice_PreBooking(checkSeatType(),customer.getMemberClass())+ " Baht");
		}
	}
	
}
