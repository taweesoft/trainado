package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import SourceCode.AddBusinessEvent;
import SourceCode.AddNormalEvent;
import SourceCode.AddPremiumEvent;
import SourceCode.CreateFlight;
import SourceCode.Customer;
import SourceCode.Database;
import SourceCode.Flight;


public class Train {
	
	protected JFrame frame;
	private ArrayList<JLabel> label_seatPremium = new ArrayList<JLabel>();
	private ArrayList<JLabel> label_seatBusiness = new ArrayList<JLabel>();
	private ArrayList<JLabel> label_seatNormal = new ArrayList<JLabel>();
	private ArrayList<String> bookedNO = new ArrayList<String>();
	private int flightIndex;
	private Flight flight;
	private Customer customer;
	private AddPremiumEvent addpremiumevent;
	private AddBusinessEvent addbusinessevent;
	private AddNormalEvent addnormalevent;
	protected int bookedSeat=0,limitSeat=0;
	private JLabel label_booked,lblBookedSeatNo;
	private JPanel panel_business,panel_premium,panel_normal;
	public Train(int stationAt,int tableIndex,Customer customer,int limitSeat) {
		this.customer = customer;
		this.limitSeat = limitSeat;
		this.flightIndex = (stationAt*3)-3 + tableIndex;
		this.flight = CreateFlight.flight[this.flightIndex];
		Database.flightIndex = flightIndex+1;
		Database.getSeatLayoutSQL();
		Database.initialNumBooking();
		initialize();
		addpremiumevent = new AddPremiumEvent(label_seatPremium,this,bookedNO);
		addbusinessevent = new AddBusinessEvent(label_seatBusiness,this,bookedNO);
		addnormalevent = new AddNormalEvent(label_seatNormal,this,bookedNO);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 153, 51));
		frame.setResizable(false);
		frame.setBounds(0, 0, 505, 690);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2 - frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel_premium = new JPanel();
		panel_premium.setBounds(10, 86, 484, 401);
		panel_premium.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_premium.setBackground(new Color(255, 204, 51));
		frame.getContentPane().add(panel_premium);
		panel_premium.setLayout(null);
		
		ClassLoader loader = this.getClass().getClassLoader();
		
		JLabel label_4 = new JLabel("");
		label_4.setBounds(49, 188, 14, 21);
		panel_premium.add(label_4);
		
		JLabel label_5 = new JLabel("");
		label_5.setBounds(73, 188, 14, 21);
		panel_premium.add(label_5);
		
		JLabel label_6 = new JLabel("");
		label_6.setBounds(150, 188, 14, 21);
		panel_premium.add(label_6);
		
		JLabel label_7 = new JLabel("");
		label_7.setBounds(171, 188, 14, 21);
		panel_premium.add(label_7);
		
		JLabel label_8 = new JLabel("");
		label_8.setBounds(49, 258, 14, 21);
		panel_premium.add(label_8);
		
		JLabel label_9 = new JLabel("");
		label_9.setBounds(73, 258, 14, 21);
		panel_premium.add(label_9);
		
		JLabel label_10 = new JLabel("");
		label_10.setBounds(150, 258, 14, 21);
		panel_premium.add(label_10);
		
		JLabel label_11 = new JLabel("");
		label_11.setBounds(171, 258, 14, 21);
		panel_premium.add(label_11);
		
		JLabel label_12 = new JLabel("");
		label_12.setBounds(49, 331, 14, 21);
		panel_premium.add(label_12);
		
		JLabel label_13 = new JLabel("");
		label_13.setBounds(73, 331, 14, 21);
		panel_premium.add(label_13);
		
		JLabel label_14 = new JLabel("");
		label_14.setBounds(150, 331, 14, 21);
		panel_premium.add(label_14);
		
		JLabel label_15 = new JLabel("");
		label_15.setBounds(171, 331, 14, 21);
		panel_premium.add(label_15);
		
		JLabel label_16 = new JLabel("");
		label_16.setBounds(290, 76, 14, 21);
		panel_premium.add(label_16);
		
		JLabel label_17 = new JLabel("");
		label_17.setBounds(314, 76, 14, 21);
		panel_premium.add(label_17);
		
		JLabel label_18 = new JLabel("");
		label_18.setBounds(391, 76, 14, 21);
		panel_premium.add(label_18);
		
		JLabel label_19 = new JLabel("");
		label_19.setBounds(412, 76, 14, 21);
		panel_premium.add(label_19);
		
		JLabel label_20 = new JLabel("");
		label_20.setBounds(290, 158, 14, 21);
		panel_premium.add(label_20);
		
		JLabel label_21 = new JLabel("");
		label_21.setBounds(314, 158, 14, 21);
		panel_premium.add(label_21);
		
		JLabel label_22 = new JLabel("");
		label_22.setBounds(391, 158, 14, 21);
		panel_premium.add(label_22);
		
		JLabel label_23 = new JLabel("");
		label_23.setBounds(412, 158, 14, 21);
		panel_premium.add(label_23);
		
		JLabel label_24 = new JLabel("");
		label_24.setBounds(290, 245, 14, 21);
		panel_premium.add(label_24);
		
		JLabel label_25 = new JLabel("");
		label_25.setBounds(314, 245, 14, 21);
		panel_premium.add(label_25);
		
		JLabel label_26 = new JLabel("");
		label_26.setBounds(391, 245, 14, 21);
		panel_premium.add(label_26);
		
		JLabel label_27 = new JLabel("");
		label_27.setBounds(412, 245, 14, 21);
		panel_premium.add(label_27);
		
		JLabel label_28 = new JLabel("");
		label_28.setBounds(290, 341, 14, 21);
		panel_premium.add(label_28);
		
		JLabel label_29 = new JLabel("");
		label_29.setBounds(314, 341, 14, 21);
		panel_premium.add(label_29);
		
		JLabel label_30 = new JLabel("");
		label_30.setBounds(391, 341, 14, 21);
		panel_premium.add(label_30);
		JLabel label_headPremium = new JLabel(new ImageIcon(loader.getResource("images/train_head.png")));
		label_headPremium.setBackground(new Color(255, 204, 51));
		label_headPremium.setBounds(35, 27, 161, 352);
		panel_premium.add(label_headPremium);
		
		JLabel label_31 = new JLabel("");
		label_31.setBounds(412, 341, 14, 21);
		panel_premium.add(label_31);
		
		JLabel label_bogiePremium = new JLabel(new ImageIcon(loader.getResource("images/premium_bogie.png")));
		label_bogiePremium.setBounds(276, 27, 161, 352);
		panel_premium.add(label_bogiePremium);
		

		panel_business = new JPanel();
		panel_business.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_business.setBackground(new Color(255, 204, 51));
		panel_business.setLayout(null);
		panel_business.setBounds(10, 86, 484, 401);
		frame.getContentPane().add(panel_business);
		
		panel_normal = new JPanel();
		panel_normal.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_normal.setBackground(new Color(255, 204, 51));
		panel_normal.setLayout(null);
		panel_normal.setBounds(10, 86, 484, 401);
		frame.getContentPane().add(panel_normal);
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(comboBox.getSelectedItem().equals("Premium Seats")){
					panel_premium.setVisible(true);
					panel_business.setVisible(false);
					panel_normal.setVisible(false);
				}else if(comboBox.getSelectedItem().equals("Business Seats")){
					panel_premium.setVisible(false);
					panel_business.setVisible(true);
					panel_normal.setVisible(false);
				}else if(comboBox.getSelectedItem().equals("Normal Seats")){
					panel_premium.setVisible(false);
					panel_business.setVisible(false);
					panel_normal.setVisible(true);
				}
			}
		});
		comboBox.setBounds(173, 37, 178, 20);
		comboBox.addItem("Premium Seats");
		comboBox.addItem("Business Seats");
		comboBox.addItem("Normal Seats");
		frame.getContentPane().add(comboBox);
		
		label_booked = new JLabel("Booked seat : 0");
		label_booked.setBounds(26, 513, 116, 14);
		frame.getContentPane().add(label_booked);
		
		JButton btnNewButton = new JButton("Book now");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(bookedSeat!=0){
				ShowTicket showticket_main = new ShowTicket(customer,flight,bookedNO);
				showticket_main.frame.setVisible(true);
				frame.setVisible(false);
				}else{
					JOptionPane.showMessageDialog(null, "Please booking");
				}
			}
		});
		btnNewButton.setBounds(132, 596, 117, 25);
		frame.getContentPane().add(btnNewButton);
		
		JLabel label_41 = new JLabel("");
		label_41.setBounds(290, 76, 14, 21);
		panel_business.add(label_41);
		
		JLabel label_42 = new JLabel("");
		label_42.setBounds(314, 76, 14, 21);
		panel_business.add(label_42);
		
		JLabel label_43 = new JLabel("");
		label_43.setBounds(391, 76, 14, 21);
		panel_business.add(label_43);
		
		JLabel label_44 = new JLabel("");
		label_44.setBounds(412, 76, 14, 21);
		panel_business.add(label_44);
		
		JLabel label_45 = new JLabel("");
		label_45.setBounds(290, 158, 14, 21);
		panel_business.add(label_45);
		
		JLabel label_46 = new JLabel("");
		label_46.setBounds(314, 158, 14, 21);
		panel_business.add(label_46);
		
		JLabel label_47 = new JLabel("");
		label_47.setBounds(391, 158, 14, 21);
		panel_business.add(label_47);
		
		JLabel label_48 = new JLabel("");
		label_48.setBounds(412, 158, 14, 21);
		panel_business.add(label_48);
		
		JLabel label_49 = new JLabel("");
		label_49.setBounds(290, 245, 14, 21);
		panel_business.add(label_49);
		
		JLabel label_50 = new JLabel("");
		label_50.setBounds(314, 245, 14, 21);
		panel_business.add(label_50);
		
		JLabel label_51 = new JLabel("");
		label_51.setBounds(391, 245, 14, 21);
		panel_business.add(label_51);
		
		JLabel label_52 = new JLabel("");
		label_52.setBounds(412, 245, 14, 21);
		panel_business.add(label_52);
		
		JLabel label_53 = new JLabel("");
		label_53.setBounds(290, 344, 14, 21);
		panel_business.add(label_53);
		
		JLabel label_54 = new JLabel("");
		label_54.setBounds(314, 344, 14, 21);
		panel_business.add(label_54);
		
		JLabel label_55 = new JLabel("");
		label_55.setBounds(391, 344, 14, 21);
		panel_business.add(label_55);
		
		JLabel label_57 = new JLabel("");
		label_57.setBounds(412, 344, 14, 21);
		panel_business.add(label_57);
		
		JLabel label_58 = new JLabel(new ImageIcon(loader.getResource("images/business_bogie.png")));
		label_58.setBounds(276, 27, 161, 352);
		panel_business.add(label_58);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(49, 76, 14, 21);
		panel_business.add(label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(73, 76, 14, 21);
		panel_business.add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(150, 76, 14, 21);
		panel_business.add(label_3);
		
		JLabel label_32 = new JLabel("");
		label_32.setBounds(171, 76, 14, 21);
		panel_business.add(label_32);
		
		JLabel label_33 = new JLabel("");
		label_33.setBounds(49, 158, 14, 21);
		panel_business.add(label_33);
		
		JLabel label_34 = new JLabel("");
		label_34.setBounds(73, 158, 14, 21);
		panel_business.add(label_34);
		
		JLabel label_35 = new JLabel("");
		label_35.setBounds(150, 158, 14, 21);
		panel_business.add(label_35);
		
		JLabel label_36 = new JLabel("");
		label_36.setBounds(171, 158, 14, 21);
		panel_business.add(label_36);
		
		JLabel label_37 = new JLabel("");
		label_37.setBounds(49, 245, 14, 21);
		panel_business.add(label_37);
		
		JLabel label_38 = new JLabel("");
		label_38.setBounds(73, 245, 14, 21);
		panel_business.add(label_38);
		
		JLabel label_39 = new JLabel("");
		label_39.setBounds(150, 245, 14, 21);
		panel_business.add(label_39);
		
		JLabel label_40 = new JLabel("");
		label_40.setBounds(171, 245, 14, 21);
		panel_business.add(label_40);
		
		JLabel label_56 = new JLabel("");
		label_56.setBounds(49, 344, 14, 21);
		panel_business.add(label_56);
		
		JLabel label_59 = new JLabel("");
		label_59.setBounds(73, 344, 14, 21);
		panel_business.add(label_59);
		
		JLabel label_60 = new JLabel("");
		label_60.setBounds(150, 344, 14, 21);
		panel_business.add(label_60);
		
		JLabel label_61 = new JLabel("");
		label_61.setBounds(171, 344, 14, 21);
		panel_business.add(label_61);
		
		JLabel label_62 = new JLabel(new ImageIcon(loader.getResource("images/business_bogie.png")));
		label_62.setBounds(35, 27, 161, 352);
		panel_business.add(label_62);
		
		JLabel lblA = new JLabel("");
		lblA.setBounds(49, 55, 14, 21);
		panel_normal.add(lblA);
		
		JLabel lblA_1 = new JLabel("");
		lblA_1.setBounds(73, 55, 14, 21);
		panel_normal.add(lblA_1);
		
		JLabel lblA_2 = new JLabel("");
		lblA_2.setBounds(150, 55, 14, 21);
		panel_normal.add(lblA_2);
		
		JLabel lblA_3 = new JLabel("");
		lblA_3.setBounds(171, 55, 14, 21);
		panel_normal.add(lblA_3);
		
		JLabel lblA_4 = new JLabel("");
		lblA_4.setBounds(49, 101, 14, 21);
		panel_normal.add(lblA_4);
		
		JLabel lblA_5 = new JLabel("");
		lblA_5.setBounds(73, 101, 14, 21);
		panel_normal.add(lblA_5);
		
		JLabel lblA_6 = new JLabel("");
		lblA_6.setBounds(150, 101, 14, 21);
		panel_normal.add(lblA_6);
		
		JLabel lblA_7 = new JLabel("");
		lblA_7.setBounds(171, 101, 14, 21);
		panel_normal.add(lblA_7);
		
		JLabel lblA_8 = new JLabel("");
		lblA_8.setBounds(49, 145, 14, 21);
		panel_normal.add(lblA_8);
		
		JLabel lblA_9 = new JLabel("");
		lblA_9.setBounds(73, 145, 14, 21);
		panel_normal.add(lblA_9);
		
		JLabel lblA_10 = new JLabel("");
		lblA_10.setBounds(150, 145, 14, 21);
		panel_normal.add(lblA_10);
		
		JLabel lblA_11 = new JLabel("");
		lblA_11.setBounds(171, 145, 14, 21);
		panel_normal.add(lblA_11);
		
		JLabel lblA_12 = new JLabel("");
		lblA_12.setBounds(49, 233, 14, 21);
		panel_normal.add(lblA_12);
		
		JLabel lblA_13 = new JLabel("");
		lblA_13.setBounds(73, 233, 14, 21);
		panel_normal.add(lblA_13);
		
		JLabel lblA_14 = new JLabel("");
		lblA_14.setBounds(150, 233, 14, 21);
		panel_normal.add(lblA_14);
		
		JLabel lblA_15 = new JLabel("");
		lblA_15.setBounds(171, 233, 14, 21);
		panel_normal.add(lblA_15);
		
		JLabel lblA_16 = new JLabel("");
		lblA_16.setBounds(49, 279, 14, 21);
		panel_normal.add(lblA_16);
		
		JLabel lblA_17 = new JLabel("");
		lblA_17.setBounds(73, 279, 14, 21);
		panel_normal.add(lblA_17);
		
		JLabel lblA_18 = new JLabel("");
		lblA_18.setBounds(150, 279, 14, 21);
		panel_normal.add(lblA_18);
		
		JLabel lblA_19 = new JLabel("");
		lblA_19.setBounds(171, 279, 14, 21);
		panel_normal.add(lblA_19);
		
		JLabel lblA_20 = new JLabel("");
		lblA_20.setBounds(49, 325, 14, 21);
		panel_normal.add(lblA_20);
		
		JLabel lblA_21 = new JLabel("");
		lblA_21.setBounds(73, 325, 14, 21);
		panel_normal.add(lblA_21);
		
		JLabel lblA_22 = new JLabel("");
		lblA_22.setBounds(150, 325, 14, 21);
		panel_normal.add(lblA_22);
		
		JLabel lblA_23 = new JLabel("");
		lblA_23.setBounds(171, 325, 14, 21);
		panel_normal.add(lblA_23);
		
		JLabel label_90 = new JLabel(new ImageIcon(loader.getResource("images/normal_bogie.png")));
		label_90.setBounds(35, 27, 161, 352);
		panel_normal.add(label_90);
		
		JLabel label_63 = new JLabel("");
		label_63.setBounds(289, 55, 14, 21);
		panel_normal.add(label_63);
		
		JLabel label_64 = new JLabel("");
		label_64.setBounds(313, 55, 14, 21);
		panel_normal.add(label_64);
		
		JLabel label_65 = new JLabel("");
		label_65.setBounds(390, 55, 14, 21);
		panel_normal.add(label_65);
		
		JLabel label_66 = new JLabel("");
		label_66.setBounds(411, 55, 14, 21);
		panel_normal.add(label_66);
		
		JLabel label_67 = new JLabel("");
		label_67.setBounds(289, 101, 14, 21);
		panel_normal.add(label_67);
		
		JLabel label_68 = new JLabel("");
		label_68.setBounds(313, 101, 14, 21);
		panel_normal.add(label_68);
		
		JLabel label_69 = new JLabel("");
		label_69.setBounds(390, 101, 14, 21);
		panel_normal.add(label_69);
		
		JLabel label_70 = new JLabel("");
		label_70.setBounds(411, 101, 14, 21);
		panel_normal.add(label_70);
		
		JLabel label_71 = new JLabel("");
		label_71.setBounds(289, 145, 14, 21);
		panel_normal.add(label_71);
		
		JLabel label_72 = new JLabel("");
		label_72.setBounds(313, 145, 14, 21);
		panel_normal.add(label_72);
		
		JLabel label_73 = new JLabel("");
		label_73.setBounds(390, 145, 14, 21);
		panel_normal.add(label_73);
		
		JLabel label_74 = new JLabel("");
		label_74.setBounds(411, 145, 14, 21);
		panel_normal.add(label_74);
		
		JLabel label_75 = new JLabel("");
		label_75.setBounds(289, 233, 14, 21);
		panel_normal.add(label_75);
		
		JLabel label_76 = new JLabel("");
		label_76.setBounds(313, 233, 14, 21);
		panel_normal.add(label_76);
		
		JLabel label_77 = new JLabel("");
		label_77.setBounds(390, 233, 14, 21);
		panel_normal.add(label_77);
		
		JLabel label_78 = new JLabel("");
		label_78.setBounds(411, 233, 14, 21);
		panel_normal.add(label_78);
		
		JLabel label_79 = new JLabel("");
		label_79.setBounds(289, 279, 14, 21);
		panel_normal.add(label_79);
		
		JLabel label_80 = new JLabel("");
		label_80.setBounds(313, 279, 14, 21);
		panel_normal.add(label_80);
		
		JLabel label_81 = new JLabel("");
		label_81.setBounds(390, 279, 14, 21);
		panel_normal.add(label_81);
		
		JLabel label_82 = new JLabel("");
		label_82.setBounds(411, 279, 14, 21);
		panel_normal.add(label_82);
		
		JLabel label_83 = new JLabel("");
		label_83.setBounds(289, 325, 14, 21);
		panel_normal.add(label_83);
		
		JLabel label_84 = new JLabel("");
		label_84.setBounds(313, 325, 14, 21);
		panel_normal.add(label_84);
		
		JLabel label_85 = new JLabel("");
		label_85.setBounds(390, 325, 14, 21);
		panel_normal.add(label_85);
		
		JLabel label_86 = new JLabel("");
		label_86.setBounds(411, 325, 14, 21);
		panel_normal.add(label_86);
		
		JLabel label_92 = new JLabel(new ImageIcon(loader.getResource("images/normal_bogie.png")));
		label_92.setBounds(276, 27, 161, 352);
		panel_normal.add(label_92);
		
		label_seatPremium.add(label_4);
		label_seatPremium.add(label_5);
		label_seatPremium.add(label_6);
		label_seatPremium.add(label_7);
		label_seatPremium.add(label_8);
		label_seatPremium.add(label_9);
		label_seatPremium.add(label_10);
		label_seatPremium.add(label_11);
		label_seatPremium.add(label_12);
		label_seatPremium.add(label_13);
		label_seatPremium.add(label_14);
		label_seatPremium.add(label_15);
		label_seatPremium.add(label_16);
		label_seatPremium.add(label_17);
		label_seatPremium.add(label_18);
		label_seatPremium.add(label_19);
		label_seatPremium.add(label_20);
		label_seatPremium.add(label_21);
		label_seatPremium.add(label_22);
		label_seatPremium.add(label_23);
		label_seatPremium.add(label_24);
		label_seatPremium.add(label_25);
		label_seatPremium.add(label_26);
		label_seatPremium.add(label_27);
		label_seatPremium.add(label_28);
		label_seatPremium.add(label_29);
		label_seatPremium.add(label_30);
		label_seatPremium.add(label_31);
		
		label_seatBusiness.add(label_1);
		label_seatBusiness.add(label_2);
		label_seatBusiness.add(label_3);
		label_seatBusiness.add(label_32);
		label_seatBusiness.add(label_33);
		label_seatBusiness.add(label_34);
		label_seatBusiness.add(label_35);
		label_seatBusiness.add(label_36);
		label_seatBusiness.add(label_37);
		label_seatBusiness.add(label_38);
		label_seatBusiness.add(label_39);
		label_seatBusiness.add(label_40);
		label_seatBusiness.add(label_56);
		label_seatBusiness.add(label_59);
		label_seatBusiness.add(label_60);
		label_seatBusiness.add(label_61);
		label_seatBusiness.add(label_41);
		label_seatBusiness.add(label_42);
		label_seatBusiness.add(label_43);
		label_seatBusiness.add(label_44);
		label_seatBusiness.add(label_45);
		label_seatBusiness.add(label_46);
		label_seatBusiness.add(label_47);
		label_seatBusiness.add(label_48);
		label_seatBusiness.add(label_49);
		label_seatBusiness.add(label_50);
		label_seatBusiness.add(label_51);
		label_seatBusiness.add(label_52);
		label_seatBusiness.add(label_53);
		label_seatBusiness.add(label_54);
		label_seatBusiness.add(label_55);
		label_seatBusiness.add(label_57);
		
		label_seatNormal.add(lblA);
		label_seatNormal.add(lblA_1);
		label_seatNormal.add(lblA_2);
		label_seatNormal.add(lblA_3);
		label_seatNormal.add(lblA_4);
		label_seatNormal.add(lblA_5);
		label_seatNormal.add(lblA_6);
		label_seatNormal.add(lblA_7);
		label_seatNormal.add(lblA_8);
		label_seatNormal.add(lblA_9);
		label_seatNormal.add(lblA_10);
		label_seatNormal.add(lblA_11);
		label_seatNormal.add(lblA_12);
		label_seatNormal.add(lblA_13);
		label_seatNormal.add(lblA_14);
		label_seatNormal.add(lblA_15);
		label_seatNormal.add(lblA_16);
		label_seatNormal.add(lblA_17);
		label_seatNormal.add(lblA_18);
		label_seatNormal.add(lblA_19);
		label_seatNormal.add(lblA_20);
		label_seatNormal.add(lblA_21);
		label_seatNormal.add(lblA_22);
		label_seatNormal.add(lblA_23);
		label_seatNormal.add(label_63);
		label_seatNormal.add(label_64);
		label_seatNormal.add(label_65);
		label_seatNormal.add(label_66);
		label_seatNormal.add(label_67);
		label_seatNormal.add(label_68);
		label_seatNormal.add(label_69);
		label_seatNormal.add(label_70);
		label_seatNormal.add(label_71);
		label_seatNormal.add(label_72);
		label_seatNormal.add(label_73);
		label_seatNormal.add(label_74);
		label_seatNormal.add(label_75);
		label_seatNormal.add(label_76);
		label_seatNormal.add(label_77);
		label_seatNormal.add(label_78);
		label_seatNormal.add(label_79);
		label_seatNormal.add(label_80);
		label_seatNormal.add(label_81);
		label_seatNormal.add(label_82);
		label_seatNormal.add(label_83);
		label_seatNormal.add(label_84);
		label_seatNormal.add(label_85);
		label_seatNormal.add(label_86);
		
		lblBookedSeatNo = new JLabel("Booked seat NO : -");
		lblBookedSeatNo.setBounds(26, 559, 462, 14);
		frame.getContentPane().add(lblBookedSeatNo);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TimeTable.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(259, 596, 117, 25);
		frame.getContentPane().add(btnBack);
		
		setImagePremium();
		setImageBusiness();
		setImageNormal();
	}
	public void setImagePremium(){
		String address="";
		for(int i =0;i<Database.getSeatLayoutRefPremium().length;i++){
			if(Database.getSeatLayoutRefPremium()[i].equals("0"))
				address="images/seat_green.png";
			else
				address = "images/seat_red.png";
			ClassLoader loader = this.getClass().getClassLoader();
			label_seatPremium.get(i).setIcon(new ImageIcon(loader.getResource(address)));
		}
	}
	public void setImageBusiness(){
		String address="";
		for(int i =0;i<Database.getSeatLayoutRefBusiness().length;i++){
			if(Database.getSeatLayoutRefBusiness()[i].equals("0"))
				address = "images/seat_green.png";
			else
				address = "images/seat_red.png";
			ClassLoader loader = this.getClass().getClassLoader();
			label_seatBusiness.get(i).setIcon(new ImageIcon(loader.getResource(address)));
		}
	}
	public void setImageNormal(){
		String address="";
		for(int i=0;i<Database.getSeatLayoutRefNormal().length;i++){
			if(Database.getSeatLayoutRefNormal()[i].equals("0"))
				address = "images/seat_green.png";
			else
				address = "images/seat_red.png";
			ClassLoader loader = this.getClass().getClassLoader();
			label_seatNormal.get(i).setIcon(new ImageIcon(loader.getResource(address)));
		}
	}
	public int updateBookedSeat(int option){
		if(option==1 && limitSeat==bookedSeat){
			return -1;
		}
		if(option==1){
			bookedSeat++;
			label_booked.setText("Booked seat : " + bookedSeat);
		}else if(option==2){
			String NO ="";
			for(int i=0;i<bookedNO.size();i++){
				NO += bookedNO.get(i)+" ";
			}
			lblBookedSeatNo.setText("Booked seat NO : "+ NO);
		}else if(option==3){
			bookedSeat--;
			label_booked.setText("Booked seat : " + bookedSeat);
		}
		
		return 1;
	}
}
