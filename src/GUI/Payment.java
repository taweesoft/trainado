package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import SourceCode.Customer;
import SourceCode.Flight;
import SourceCode.Store;


public class Payment {

	protected JFrame frame;
	private double totalPrice;
	private Customer customer;
	private Flight flight;
	private static int bookedSeat;
	private ClassLoader loader = this.getClass().getClassLoader();
	private ArrayList<String> bookedNO;
	private JLabel label_confirm;
	private JLabel label_later;
	public Payment(double totalPrice,Customer customer,Flight flight,int bookedSeat,ArrayList<String> bookedNO) {
		this.totalPrice = totalPrice;
		this.customer = customer;
		this.flight = flight;
		this.bookedSeat = bookedSeat;
		this.bookedNO = bookedNO;
		initialize();
	}

	private void initialize() {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		
		frame = new JFrame();
		frame.setUndecorated(true);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setResizable(false);
		frame.setBounds(0, 0, 431, 510);
		frame.setLocation(dimension.width/2 - frame.getWidth()/2, dimension.height/2 - frame.getHeight()/2);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		label_confirm = new JLabel(new ImageIcon(loader.getResource("images/confirm1.png")));
		label_confirm.addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent arg0) {}
			public void mouseEntered(MouseEvent e) {
				label_confirm.setIcon(new ImageIcon(loader.getResource("images/confirm2.png")));
			}
			public void mouseExited(MouseEvent e) {
				label_confirm.setIcon(new ImageIcon(loader.getResource("images/confirm1.png")));
			}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {
				Store.receivePayment(flight, customer);
				JOptionPane.showMessageDialog(null, "Thank you for booking","Trainado",JOptionPane.INFORMATION_MESSAGE);
				Main.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		label_confirm.setBounds(10, 415, 204, 66);
		frame.getContentPane().add(label_confirm);
		
		JLabel label_customerName = new JLabel("Name : " +customer.getName().toUpperCase() + "." +Character.toString(customer.getLastname().charAt(0)).toUpperCase());
		label_customerName.setFont(new Font("Courier New", Font.BOLD, 20));
		label_customerName.setBounds(14, 75, 404, 36);
		frame.getContentPane().add(label_customerName);
		
		JLabel label_flight = new JLabel("Flight : " + flight.getFlightInfo().getInfo());
		label_flight.setFont(new Font("Courier New", Font.BOLD, 16));
		label_flight.setBounds(14, 136, 268, 36);
		frame.getContentPane().add(label_flight);
		
		JLabel label_fromTo = new JLabel("From : " + flight.getFlightInfo().getFromCity() + " To : " + flight.getFlightInfo().getToCity());
		label_fromTo.setFont(new Font("Courier New", Font.BOLD, 16));
		label_fromTo.setBounds(14, 183, 413, 36);
		frame.getContentPane().add(label_fromTo);
		
		JLabel label_seatNO = new JLabel("Seat(s) : " + Arrays.toString(bookedNO.toArray()).substring(1, Arrays.toString(bookedNO.toArray()).length()-1).replace(",", ""));
		label_seatNO.setFont(new Font("Courier New", Font.BOLD, 16));
		label_seatNO.setBounds(14, 230, 404, 36);
		frame.getContentPane().add(label_seatNO);
		
		JLabel label_price = new JLabel(String.format("Price : %.0f", totalPrice));
		label_price.setFont(new Font("Courier New", Font.BOLD, 16));
		label_price.setBounds(214, 277, 213, 36);
		frame.getContentPane().add(label_price);
		
		JLabel label_bill = new JLabel(new ImageIcon(loader.getResource("images/bill.jpg")));
		label_bill.setBounds(0, 0, 427, 404);
		frame.getContentPane().add(label_bill);
		
		label_later = new JLabel(new ImageIcon(loader.getResource("images/later1.png")));
		label_later.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent e){
				label_later.setIcon(new ImageIcon(loader.getResource("images/later2.gif")));
			}
			public void mouseExited(MouseEvent e){
				label_later.setIcon(new ImageIcon(loader.getResource("images/later1.png")));
			}
			public void mouseReleased(MouseEvent e){
				Main.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		label_later.setBounds(214, 415, 204, 66);
		frame.getContentPane().add(label_later);
		
	}
}
