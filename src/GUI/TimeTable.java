package GUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import SourceCode.CreateFlight;
import SourceCode.Customer;
import SourceCode.Database;
import SourceCode.Flight;
import SourceCode.Person;


public class TimeTable {

	public static JFrame frame;
	private JTable table;
	private JTextField txt_lastname;
	private JTextField txt_name;
	private JTextField txt_tel;
	private JTextField txt_email;
	private JTextField txt_username;
	private JPasswordField txt_password;
	private String[][] timeTableArr;
	private String[] timeTableHeader = {"From" , "To","Time","Flight"};
	private int stationAt;
	private Flight flight;
	private JTextField txt_regisName;
	private JTextField txt_regisLastname;
	private JTextField txt_regisTel;
	private JTextField txt_regisEmail;
	private JTextField txt_regisUser;
	private JPasswordField txt_regisPass;
	private JPanel panel_regis,panel_login;
	private JTextField txt_customerID;
	private JComboBox<String> cbb_gender;
	private JTextField txt_memberclass;
	private boolean alreadyLogin = false;
	/**
	 * Launch the application.
	 */
	public void setTimeTableArr(){
		
		timeTableArr = new String[3][4];
		int start=0,end=0;
		switch(stationAt){
		case 1:
			start=0;
			end=0;
			break;
		case 2:
			start=3;
			break;
		case 3:
			start=6;
			break;
		case 4: 
			start=9;
			break;
		case 5:
			start=12;
			break;
		}
		for(int i=0;i<timeTableArr.length;i++){
			for(int j=0;j<timeTableArr[i].length;j++){
				if(j==0)
					timeTableArr[i][j] = CreateFlight.flightInfo[start+i].getFromCity();
				else if(j==1)
					timeTableArr[i][j] = CreateFlight.flightInfo[start+i].getToCity();
				else if(j==2)
					timeTableArr[i][j] = CreateFlight.flightInfo[start+i].getFromtime().toString() + "-" + CreateFlight.flightInfo[start+i].getTotime().toString();
				else
					timeTableArr[i][j] = CreateFlight.flightInfo[start+i].getInfo();
			}
		}
		
	}

	/**
	 * Create the application.
	 */
	public TimeTable(int stationAt) {
		this.stationAt = stationAt;
		setTimeTableArr();
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBackground(Color.WHITE);
		frame.setResizable(false);
		frame.setBounds(0, 0, 487, 690);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dimension.width/2 - frame.getWidth()/2,dimension.height/2 - frame.getHeight()/2);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 72, 437, 71);
		panel.add(scrollPane);
		
		table = new JTable(timeTableArr,timeTableHeader);
		scrollPane.setViewportView(table);
		
		
		panel_login = new JPanel();
		panel_login.setBackground(Color.WHITE);
		panel_login.setBounds(250, 180, 230, 224);
		panel.add(panel_login);
		panel_login.setLayout(null);
		
		JButton btn_signup = new JButton("Sign up");
		btn_signup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_regis.setVisible(true);
				panel_login.setVisible(false);
			}
		});
		btn_signup.setBounds(124, 165, 89, 23);
		panel_login.add(btn_signup);
		
		JButton btn_login = new JButton("Login");
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				checkLogin();
				txt_username.setText("");
				txt_password.setText("");
			}
		});
		btn_login.setBounds(25, 165, 89, 23);
		panel_login.add(btn_login);
		
		txt_password = new JPasswordField();
		txt_password.setBounds(104, 114, 97, 20);
		panel_login.add(txt_password);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(21, 117, 73, 14);
		panel_login.add(lblPassword);
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(21, 66, 73, 14);
		panel_login.add(lblUsername);
		lblUsername.setHorizontalAlignment(SwingConstants.RIGHT);
		
		txt_username = new JTextField();
		txt_username.setBounds(104, 63, 97, 20);
		panel_login.add(txt_username);
		txt_username.setColumns(10);
		
		JLabel lblMemberOnly = new JLabel("Member only");
		lblMemberOnly.setBounds(73, 11, 109, 14);
		panel_login.add(lblMemberOnly);
		
		panel_regis = new JPanel();
		panel_regis.setBackground(Color.WHITE);
		panel_regis.setBounds(250, 180, 230, 460);
		panel.add(panel_regis);
		panel_regis.setLayout(null);
		
		JLabel label = new JLabel("Firstname");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(21, 171, 73, 14);
		panel_regis.add(label);
		
		txt_regisName = new JTextField();
		txt_regisName.setColumns(10);
		txt_regisName.setBounds(104, 168, 97, 20);
		panel_regis.add(txt_regisName);
		
		JLabel label_1 = new JLabel("Lastname");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setBounds(21, 226, 73, 14);
		panel_regis.add(label_1);
		
		txt_regisLastname = new JTextField();
		txt_regisLastname.setColumns(10);
		txt_regisLastname.setBounds(104, 223, 97, 20);
		panel_regis.add(txt_regisLastname);
		
		JLabel label_2 = new JLabel("Telephone");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setBounds(12, 336, 82, 14);
		panel_regis.add(label_2);
		
		txt_regisTel = new JTextField();
		txt_regisTel.setColumns(10);
		txt_regisTel.setBounds(104, 333, 97, 20);
		panel_regis.add(txt_regisTel);
		
		JLabel label_3 = new JLabel("Email");
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setBounds(21, 390, 73, 14);
		panel_regis.add(label_3);
		
		txt_regisEmail = new JTextField();
		txt_regisEmail.setColumns(10);
		txt_regisEmail.setBounds(104, 387, 97, 20);
		panel_regis.add(txt_regisEmail);
		
		txt_regisUser = new JTextField();
		txt_regisUser.setColumns(10);
		txt_regisUser.setBounds(104, 63, 97, 20);
		panel_regis.add(txt_regisUser);
		
		JLabel label_4 = new JLabel("Username");
		label_4.setHorizontalAlignment(SwingConstants.RIGHT);
		label_4.setBounds(21, 66, 73, 14);
		panel_regis.add(label_4);
		
		JLabel label_5 = new JLabel("Password");
		label_5.setHorizontalAlignment(SwingConstants.RIGHT);
		label_5.setBounds(21, 117, 73, 14);
		panel_regis.add(label_5);
		
		txt_regisPass = new JPasswordField();
		txt_regisPass.setBounds(104, 114, 97, 20);
		panel_regis.add(txt_regisPass);
		
		JButton btn_regis = new JButton("Sign up");
		btn_regis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				signUp();
				resetAllTextField();
			}
		});
		btn_regis.setBounds(21, 426, 89, 23);
		panel_regis.add(btn_regis);
		
		JButton btn_regisCancel = new JButton("Back");
		btn_regisCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_regis.setVisible(false);
				panel_login.setVisible(true);
				resetAllTextField();
			}
		});
		btn_regisCancel.setBounds(117, 426, 89, 23);
		panel_regis.add(btn_regisCancel);
		
		JLabel lb_regis = new JLabel("Register");
		lb_regis.setHorizontalAlignment(SwingConstants.CENTER);
		lb_regis.setBounds(61, 11, 109, 14);
		panel_regis.add(lb_regis);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGender.setBounds(21, 285, 73, 14);
		panel_regis.add(lblGender);
		
		cbb_gender = new JComboBox<String>();
		cbb_gender.setBounds(104, 282, 97, 20);
		cbb_gender.addItem("Male");
		cbb_gender.addItem("Female");
		panel_regis.add(cbb_gender);
		panel_regis.setVisible(false);
		
		JPanel panel_nonmember = new JPanel();
		panel_nonmember.setBackground(Color.WHITE);
		panel_nonmember.setBounds(10, 180, 230, 460);
		panel.add(panel_nonmember);
		panel_nonmember.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("non-member");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(74, 11, 109, 14);
		panel_nonmember.add(lblNewLabel);
		
		JLabel lblPersons = new JLabel("Person(s)");
		lblPersons.setBounds(21, 68, 73, 14);
		panel_nonmember.add(lblPersons);
		lblPersons.setHorizontalAlignment(SwingConstants.RIGHT);
		
		final JComboBox<Integer> cbb_persons = new JComboBox<Integer>();
		cbb_persons.setBounds(104, 65, 102, 20);
		panel_nonmember.add(cbb_persons);
		for(int i =1;i<=10;i++) cbb_persons.addItem(i);
		
		JLabel lblFirstname = new JLabel("Firstname");
		lblFirstname.setBounds(12, 174, 82, 14);
		panel_nonmember.add(lblFirstname);
		lblFirstname.setHorizontalAlignment(SwingConstants.RIGHT);
		
		txt_name = new JTextField();
		txt_name.setBounds(104, 171, 102, 20);
		panel_nonmember.add(txt_name);
		txt_name.setColumns(10);
		
		txt_lastname = new JTextField();
		txt_lastname.setBounds(104, 226, 102, 20);
		panel_nonmember.add(txt_lastname);
		txt_lastname.setColumns(10);
		
		JLabel lblLastname = new JLabel("Lastname");
		lblLastname.setBounds(12, 229, 82, 14);
		panel_nonmember.add(lblLastname);
		lblLastname.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel lblTelephone = new JLabel("Telephone");
		lblTelephone.setBounds(12, 285, 82, 14);
		panel_nonmember.add(lblTelephone);
		lblTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
		
		txt_tel = new JTextField();
		txt_tel.setBounds(104, 282, 102, 20);
		panel_nonmember.add(txt_tel);
		txt_tel.setColumns(10);
		
		txt_email = new JTextField();
		txt_email.setBounds(104, 336, 102, 20);
		panel_nonmember.add(txt_email);
		txt_email.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(21, 339, 73, 14);
		panel_nonmember.add(lblEmail);
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JButton btn_book = new JButton("Book");
		btn_book.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Customer customer =null;
			
					if(!txt_name.getText().equals("") && !txt_lastname.getText().equals("") && !txt_email.getText().equals("") && !txt_tel.getText().equals("") && table.getSelectedRow()!=-1){
						if(alreadyLogin){
							for(int i=0;i<Person.getCustomerList().size();i++){
								if(Person.getCustomerList().get(i).equals(txt_name.getText(), txt_lastname.getText())){
									customer = Person.getCustomerList().get(i);
								}
							}
						}else{
							customer = new Customer(txt_customerID.getText(),
								txt_name.getText(),
								txt_lastname.getText(),
								txt_tel.getText(),
								txt_email.getText(),
								(String)cbb_gender.getSelectedItem(),
								Integer.parseInt(txt_memberclass.getText()));
						Person.getCustomerList().add(customer);
						}
						Train train_main = new Train(stationAt,table.getSelectedRow(),customer,(int)cbb_persons.getSelectedItem());
						train_main.frame.setVisible(true);
						frame.setVisible(false);
				
				}else{
					JOptionPane.showMessageDialog(null, "Information can't be blank");
				}
			}
		});
		btn_book.setBounds(21, 426, 89, 23);
		panel_nonmember.add(btn_book);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				Map map_main = new Map();
				map_main.frame.setVisible(true);
			}
		});
		btnBack.setBounds(117, 426, 89, 23);
		panel_nonmember.add(btnBack);
		
		JLabel lblCustomerid = new JLabel("CustomerID");
		lblCustomerid.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCustomerid.setBounds(12, 117, 82, 14);
		panel_nonmember.add(lblCustomerid);
		
		txt_customerID = new JTextField();
		txt_customerID.setText("non-member");
		txt_customerID.setEditable(false);
		txt_customerID.setColumns(10);
		txt_customerID.setBounds(104, 114, 102, 20);
		panel_nonmember.add(txt_customerID);
		
		JLabel lblMemberClass = new JLabel("Class");
		lblMemberClass.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMemberClass.setBounds(12, 390, 82, 14);
		panel_nonmember.add(lblMemberClass);
		
		txt_memberclass = new JTextField();
		txt_memberclass.setText("0");
		txt_memberclass.setEditable(false);
		txt_memberclass.setColumns(10);
		txt_memberclass.setBounds(104, 387, 102, 20);
		panel_nonmember.add(txt_memberclass);
		
		JLabel lblNewLabel_1 = new JLabel("Time Table");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Courier New", Font.BOLD, 39));
		lblNewLabel_1.setBounds(25, 12, 455, 39);
		panel.add(lblNewLabel_1);
	}
	public void checkLogin(){
		if(txt_username.getText() != "" && txt_password.getText()!=""){
			String username = txt_username.getText();
			String password = txt_password.getText();
			if(Database.getLoginArr().size()==0){
				JOptionPane.showMessageDialog(null, "Wrong username or password","Error",JOptionPane.ERROR_MESSAGE);
			}else{
				for(int i =0;i<Database.getLoginArr().size();i++){
					if(username.equalsIgnoreCase(Database.getLoginArr().get(i)[1])){
						if(password.equals(Database.getLoginArr().get(i)[2])){
							JOptionPane.showMessageDialog(null,"Welcome, " + Database.getLoginArr().get(i)[3],"Login successful",JOptionPane.PLAIN_MESSAGE);
							txt_customerID.setText(Database.getLoginArr().get(i)[0]);
							txt_name.setText(Database.getLoginArr().get(i)[3]);
							txt_lastname.setText(Database.getLoginArr().get(i)[4]);
							txt_tel.setText(Database.getLoginArr().get(i)[5]);
							txt_email.setText(Database.getLoginArr().get(i)[6]);
							txt_memberclass.setText(Database.getLoginArr().get(i)[8]);
							alreadyLogin= true;
							break;
						}
					}else if(i==Database.getLoginArr().size()-1){
						JOptionPane.showMessageDialog(null, "Wrong username or password","Error",JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			
		}
	}
	public void signUp(){
		String[] regisInfo = new String[9];
		regisInfo[0] = String.format("%04d", Database.getLoginArr().size()+1);
		regisInfo[1] = txt_regisUser.getText();
		regisInfo[2] = txt_regisPass.getText();
		regisInfo[3] = txt_regisName.getText();
		regisInfo[4] = txt_regisLastname.getText();
		if(checkNewMember(regisInfo[1],regisInfo[3],regisInfo[4])){
			regisInfo[5] = txt_regisTel.getText();
			regisInfo[6] = txt_regisEmail.getText();
			regisInfo[7] = (String)cbb_gender.getSelectedItem();
			regisInfo[8] = "1";
			Customer customer = new Customer(regisInfo[0],regisInfo[3],regisInfo[4],regisInfo[5],regisInfo[6],regisInfo[7],1);
			Database.getLoginArr().add(regisInfo);
			Database.saveLogin();
			JOptionPane.showMessageDialog(null,"Sign up successful", "Sign up",JOptionPane.DEFAULT_OPTION);
			panel_regis.setVisible(false);
			panel_login.setVisible(true);
		}else{
			JOptionPane.showMessageDialog(null, "Already sign up","Sign up",JOptionPane.INFORMATION_MESSAGE);
		}
		
	}
	public void resetAllTextField(){
		txt_regisUser.setText("");
		txt_regisPass.setText("");
		txt_regisName.setText("");
		txt_regisLastname.setText("");
		txt_regisTel.setText("");
		txt_regisEmail.setText("");
		txt_username.setText("");
		txt_password.setText("");
		txt_name.setText("");
		txt_lastname.setText("");
		txt_tel.setText("");
		txt_email.setText("");
	}
	public boolean checkNewMember(String username,String firstname,String lastname){
		for(int i=0;i<Database.getLoginArr().size();i++){
			if(Database.getLoginArr().get(i)[1].equalsIgnoreCase(username)){
				return false;
			}
		}
		return true;
	}
}
