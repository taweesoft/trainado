package SourceCode;


public class CreateFlight {
	public static FlightInfo[] flightInfo = new FlightInfo[15];
	public static Flight[] flight = new Flight[15];
	public Time departureBKKCHM1= new Time (12,0,0);
	public Time arrivalBKKCHM1 = new Time (20,0,0);
	public Time departureBKKCHM2 = new Time (18,0,0);
	public Time arrivalBKKCHM2 = new Time (02,0,0);
	public Time departureBKKCHM3 = new Time (9,0,0);
	public Time arrivalBKKCHM3 = new Time (16,0,0);
	
	public Time departureBKKKPP1= new Time (12,0,0);
	public Time arrivalBKKKPP1 = new Time (20,0,0);
	public Time departureBKKKPP2 = new Time (18,0,0);
	public Time arrivalBKKKPP2 = new Time (02,0,0);
	public Time departureBKKKPP3 = new Time (9,0,0);
	public Time arrivalBKKKPP3 = new Time (16,0,0);
	
	public Time departureBKKKKN1= new Time (12,0,0);
	public Time arrivalBKKKKN1 = new Time (20,0,0);
	public Time departureBKKKKN2 = new Time (18,0,0);
	public Time arrivalBKKKKN2 = new Time (02,0,0);
	public Time departureBKKKKN3 = new Time (9,0,0);
	public Time arrivalBKKKKN3 = new Time (16,0,0);
	
	public Time departureBKKHHN1= new Time (12,0,0);
	public Time arrivalBKKHHN1 = new Time (20,0,0);
	public Time departureBKKHHN2 = new Time (18,0,0);
	public Time arrivalBKKHHN2 = new Time (02,0,0);
	public Time departureBKKHHN3 = new Time (9,0,0);
	public Time arrivalBKKHHN3 = new Time (16,0,0);
	
	public Time departureBKKPKT1= new Time (12,0,0);
	public Time arrivalBKKPKT1 = new Time (20,0,0);
	public Time departureBKKPKT2 = new Time (18,0,0);
	public Time arrivalBKKPKT2 = new Time (02,0,0);
	public Time departureBKKPKT3 = new Time (9,0,0);
	public Time arrivalBKKPKT3 = new Time (16,0,0);
	
	public CreateFlight(){
		setFlightInfoArr();
	}
	public void setFlightInfoArr(){
		flightInfo[0] = new FlightInfo ("TS780", departureBKKCHM1, arrivalBKKCHM1, "Bangkok", "Chiangmai");
		flightInfo[1] = new FlightInfo ("TS760", departureBKKCHM2, arrivalBKKCHM2, "Bangkok", "Chiangmai");
		flightInfo[2] = new FlightInfo ("TS700", departureBKKCHM3, arrivalBKKCHM3, "Bangkok", "Chiangmai");

		flightInfo[3] = new FlightInfo ("TS600", departureBKKKPP1, arrivalBKKKPP1, "Bangkok", "Kampanpetch");
		flightInfo[4] = new FlightInfo ("TS610", departureBKKKPP2, arrivalBKKKPP2, "Bangkok", "Kampanpetch");
		flightInfo[5] = new FlightInfo ("TS620", departureBKKKPP3, arrivalBKKKPP3, "Bangkok", "Kampanpetch");

		flightInfo[6] = new FlightInfo ("TS710", departureBKKKKN1, arrivalBKKKKN1, "Bangkok", "Khon Kaen");
		flightInfo[7] = new FlightInfo ("TS720", departureBKKKKN2, arrivalBKKKKN2, "Bangkok", "Khon Kaen");
		flightInfo[8] = new FlightInfo ("TS730", departureBKKKKN3, arrivalBKKKKN3, "Bangkok", "Khon Kaen");

		flightInfo[9] = new FlightInfo ("TS750", departureBKKHHN1, arrivalBKKHHN1, "Bangkok", "Huahin");
		flightInfo[10] = new FlightInfo ("TS790", departureBKKHHN2, arrivalBKKHHN2, "Bangkok", "Huahin");
		flightInfo[11] = new FlightInfo ("TS740", departureBKKHHN3, arrivalBKKHHN3, "Bangkok", "Huahin");

		flightInfo[12] = new FlightInfo ("TS670", departureBKKPKT1, arrivalBKKPKT1, "Bangkok", "Phuket");
		flightInfo[13] = new FlightInfo ("TS690", departureBKKPKT2, arrivalBKKPKT2, "Bangkok", "Phuket");
		flightInfo[14] = new FlightInfo ("TS680", departureBKKPKT3, arrivalBKKPKT3, "Bangkok", "Phuket");
		
		flight[0] = new Flight (new int[] {48,32,28}, flightInfo[0], 100000, new double []{4000, 6500, 1200});
		flight[1] = new Flight (new int[] {48,32,28}, flightInfo[1], 100000, new double []{4000, 6500, 1200});
		flight[2] = new Flight (new int[] {48,32,28}, flightInfo[2], 120000, new double []{4000, 6500, 1200});
		
		flight[3] = new Flight (new int[] {48,32,28}, flightInfo[3], 100000, new double []{3000, 5500, 9000});
		flight[4] = new Flight (new int[] {48,32,28}, flightInfo[4], 100000, new double []{3000, 5500, 9000});
		flight[5] = new Flight (new int[] {48,32,28}, flightInfo[5], 120000, new double []{3000, 5500, 9000});

		flight[6] = new Flight (new int[] {48,32,28}, flightInfo[6], 100000, new double []{3000, 5500, 9000});
		flight[7] = new Flight (new int[] {48,32,28}, flightInfo[7], 100000, new double []{3000, 5500, 9000});
		flight[8] = new Flight (new int[] {48,32,28}, flightInfo[8], 120000, new double []{3000, 5500, 9000});

		flight[9] = new Flight (new int[] {48,32,28}, flightInfo[9], 100000, new double []{3000, 5500, 9000});
		flight[10] = new Flight (new int[] {48,32,28}, flightInfo[10], 100000, new double []{3000, 5500, 9000});
		flight[11] = new Flight (new int[] {48,32,28}, flightInfo[11], 120000, new double []{3000, 5500, 9000});

		flight[12] = new Flight (new int[] {48,32,28}, flightInfo[12], 100000, new double []{4000, 6000, 10000});
		flight[13] = new Flight (new int[] {48,32,28}, flightInfo[13], 100000, new double []{4000, 6000, 10000});
		flight[14] = new Flight (new int[] {48,32,28}, flightInfo[14], 120000, new double []{4000, 6000, 10000});
	}
	public static int searchFlightInfo(String info){
		for(int i=0;i<flightInfo.length;i++){
			if(flightInfo[i].getInfo().equals(info)){
				return i;
			}
		}
		return -1;
	}
}

