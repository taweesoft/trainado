package SourceCode;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import GUI.Train;


public class AddPremiumEvent {
	private ArrayList<JLabel> label_seatPremium = new ArrayList<JLabel>();
	private ArrayList<String> bookedNO;
	public AddPremiumEvent(ArrayList<JLabel> label_seatPremium,final Train train,ArrayList<String> bookedNO){
		this.label_seatPremium = label_seatPremium;
		this.bookedNO = bookedNO;
		label_seatPremium.get(0).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[0].equals("2")){
					setGreenSeat(0);
					train.updateBookedSeat(2);
					train.updateBookedSeat(3);
				}else
			if(!Database.getSeatLayoutRefPremium()[0].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[0] = "2";
				setYellowSeat(0);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(1).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[1].equals("2")){
					setGreenSeat(1);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[1].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[1] = "2";
				setYellowSeat(1);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(2).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[2].equals("2")){
					setGreenSeat(2);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[2].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[2] = "2";
				setYellowSeat(2);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(3).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[3].equals("2")){
					setGreenSeat(3);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[3].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[3] = "2";
				setYellowSeat(3);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(4).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[4].equals("2")){
					setGreenSeat(4);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[4].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[4] = "2";
				setYellowSeat(4);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(5).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[5].equals("2")){
					setGreenSeat(5);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[5].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[5] = "2";
				setYellowSeat(5);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(6).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[6].equals("2")){
					setGreenSeat(6);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[6].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[6] = "2";
				setYellowSeat(6);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(7).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[7].equals("2")){
					setGreenSeat(7);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[7].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[7] = "2";
				setYellowSeat(7);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(8).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[8].equals("2")){
					setGreenSeat(8);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[8].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[8] = "2";
				setYellowSeat(8);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(9).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[9].equals("2")){
					setGreenSeat(9);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[9].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[9] = "2";
				setYellowSeat(9);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(10).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[10].equals("2")){
					setGreenSeat(10);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[10].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[10] = "2";
				setYellowSeat(10);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(11).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[11].equals("2")){
					setGreenSeat(11);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[11].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[11] = "2";
				setYellowSeat(11);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(12).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[12].equals("2")){
					setGreenSeat(12);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[12].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[12] = "2";
				setYellowSeat(12);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(13).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[13].equals("2")){
					setGreenSeat(13);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[13].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[13] = "2";
				setYellowSeat(13);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(14).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[14].equals("2")){
					setGreenSeat(14);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[14].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[14] = "2";
				setYellowSeat(14);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(15).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[15].equals("2")){
					setGreenSeat(15);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[15].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[15] = "2";
				setYellowSeat(15);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(16).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[16].equals("2")){
					setGreenSeat(16);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[16].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[16] = "2";
				setYellowSeat(16);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(17).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[17].equals("2")){
					setGreenSeat(17);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[17].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[17] = "2";
				setYellowSeat(17);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(18).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[18].equals("2")){
					setGreenSeat(18);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[18].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[18] = "2";
				setYellowSeat(18);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(19).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[19].equals("2")){
					setGreenSeat(19);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[19].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[19] = "2";
				setYellowSeat(19);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(20).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[20].equals("2")){
					setGreenSeat(20);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[20].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[20] = "2";
				setYellowSeat(20);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(21).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[21].equals("2")){
					setGreenSeat(21);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[21].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[21] = "2";
				setYellowSeat(21);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(22).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[22].equals("2")){
					setGreenSeat(22);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[22].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[22] = "2";
				setYellowSeat(22);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(23).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[23].equals("2")){
					setGreenSeat(23);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[23].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[23] = "2";
				setYellowSeat(23);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(24).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[24].equals("2")){
					setGreenSeat(24);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[24].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[24] = "2";
				setYellowSeat(24);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(25).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[25].equals("2")){
					setGreenSeat(25);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[25].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[25] = "2";
				setYellowSeat(25);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(26).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[26].equals("2")){
					setGreenSeat(26);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[26].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[26] = "2";
				setYellowSeat(26);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatPremium.get(27).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefPremium()[27].equals("2")){
					setGreenSeat(27);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefPremium()[27].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefPremium()[27] = "2";
				setYellowSeat(27);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
	}
	public void setYellowSeat(int index){
		ClassLoader loader = this.getClass().getClassLoader();
		label_seatPremium.get(index).setIcon(new ImageIcon(loader.getResource("images/seat_yellow.png")));
		bookedNO.add("P" + (index+1));
	}
	public void setGreenSeat(int index){
		ClassLoader loader = this.getClass().getClassLoader();
		label_seatPremium.get(index).setIcon(new ImageIcon(loader.getResource("images/seat_green.png")));
		Database.getSeatLayoutRefPremium()[index]="0";
		for(int i =0;i<bookedNO.size();i++){
			if(bookedNO.get(i).equals("P" +(index+1)))
				bookedNO.remove(i);
		}
	}
	}

