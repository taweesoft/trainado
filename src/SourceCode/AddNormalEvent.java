package SourceCode;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import GUI.Train;


public class AddNormalEvent {
	private ArrayList<JLabel> label_seatNormal = new ArrayList<JLabel>();
	private ArrayList<String> bookedNO = new ArrayList<String>();
	public AddNormalEvent(ArrayList<JLabel> label_seatNormal,final Train train,ArrayList<String> bookedNO){
		this.label_seatNormal = label_seatNormal;
		this.bookedNO = bookedNO;
		label_seatNormal.get(0).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[0].equals("2")){
					setGreenSeat(0);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else
			if(!Database.getSeatLayoutRefNormal()[0].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[0] = "2";
				setYellowSeat(0);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(1).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[1].equals("2")){
					setGreenSeat(1);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[1].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[1] = "2";
				setYellowSeat(1);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(2).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[2].equals("2")){
					setGreenSeat(2);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[2].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[2] = "2";
				setYellowSeat(2);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(3).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[3].equals("2")){
					setGreenSeat(3);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[3].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[3] = "2";
				setYellowSeat(3);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(4).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[4].equals("2")){
					setGreenSeat(4);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[4].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[4] = "2";
				setYellowSeat(4);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(5).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[5].equals("2")){
					setGreenSeat(5);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[5].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[5] = "2";
				setYellowSeat(5);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(6).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[6].equals("2")){
					setGreenSeat(6);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[6].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[6] = "2";
				setYellowSeat(6);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(7).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[7].equals("2")){
					setGreenSeat(7);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[7].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[7] = "2";
				setYellowSeat(7);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(8).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[8].equals("2")){
					setGreenSeat(8);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[8].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[8] = "2";
				setYellowSeat(8);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(9).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[9].equals("2")){
					setGreenSeat(9);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[9].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[9] = "2";
				setYellowSeat(9);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(10).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[10].equals("2")){
					setGreenSeat(10);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[10].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[10] = "2";
				setYellowSeat(10);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(11).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[11].equals("2")){
					setGreenSeat(11);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[11].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[11] = "2";
				setYellowSeat(11);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(12).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[12].equals("2")){
					setGreenSeat(12);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[12].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[12] = "2";
				setYellowSeat(12);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(13).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[13].equals("2")){
					setGreenSeat(13);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[13].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[13] = "2";
				setYellowSeat(13);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(14).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[14].equals("2")){
					setGreenSeat(14);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[14].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[14] = "2";
				setYellowSeat(14);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(15).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[15].equals("2")){
					setGreenSeat(15);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[15].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[15] = "2";
				setYellowSeat(15);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(16).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[16].equals("2")){
					setGreenSeat(16);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[16].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[16] = "2";
				setYellowSeat(16);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(17).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[17].equals("2")){
					setGreenSeat(17);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[17].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[17] = "2";
				setYellowSeat(17);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(18).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[18].equals("2")){
					setGreenSeat(18);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[18].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[18] = "2";
				setYellowSeat(18);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(19).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[19].equals("2")){
					setGreenSeat(19);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[19].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[19] = "2";
				setYellowSeat(19);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(20).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[20].equals("2")){
					setGreenSeat(20);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[20].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[20] = "2";
				setYellowSeat(20);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(21).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[21].equals("2")){
					setGreenSeat(21);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[21].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[21] = "2";
				setYellowSeat(21);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(22).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[22].equals("2")){
					setGreenSeat(22);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[22].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[22] = "2";
				setYellowSeat(22);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(23).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[23].equals("2")){
					setGreenSeat(23);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[23].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[23] = "2";
				setYellowSeat(23);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(24).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[24].equals("2")){
					setGreenSeat(24);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[24].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[24] = "2";
				setYellowSeat(24);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(25).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[25].equals("2")){
					setGreenSeat(25);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[25].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[25] = "2";
				setYellowSeat(25);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(26).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[26].equals("2")){
					setGreenSeat(26);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[26].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[26] = "2";
				setYellowSeat(26);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(27).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[27].equals("2")){
					setGreenSeat(27);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[27].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[27] = "2";
				setYellowSeat(27);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(28).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[28].equals("2")){
					setGreenSeat(28);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[28].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[28] = "2";
				setYellowSeat(28);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(29).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[29].equals("2")){
					setGreenSeat(29);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[29].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[29] = "2";
				setYellowSeat(29);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(30).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[30].equals("2")){
					setGreenSeat(30);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[30].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[30] = "2";
				setYellowSeat(30);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(31).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[31].equals("2")){
					setGreenSeat(31);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[31].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[31] = "2";
				setYellowSeat(31);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(32).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[32].equals("2")){
					setGreenSeat(32);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[32].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[32] = "2";
				setYellowSeat(32);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(33).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[33].equals("2")){
					setGreenSeat(33);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[33].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[33] = "2";
				setYellowSeat(33);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(34).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[34].equals("2")){
					setGreenSeat(34);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[34].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[34] = "2";
				setYellowSeat(34);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(35).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[35].equals("2")){
					setGreenSeat(35);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[35].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[35] = "2";
				setYellowSeat(35);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(36).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[36].equals("2")){
					setGreenSeat(36);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[36].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[36] = "2";
				setYellowSeat(36);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(37).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[37].equals("2")){
					setGreenSeat(37);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[37].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[37] = "2";
				setYellowSeat(37);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(38).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[38].equals("2")){
					setGreenSeat(38);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[38].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[38] = "2";
				setYellowSeat(38);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(39).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[39].equals("2")){
					setGreenSeat(39);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[39].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[39] = "2";
				setYellowSeat(39);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(40).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[40].equals("2")){
					setGreenSeat(40);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[40].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[40] = "2";
				setYellowSeat(40);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(41).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[41].equals("2")){
					setGreenSeat(41);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[41].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[41] = "2";
				setYellowSeat(41);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(42).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[42].equals("2")){
					setGreenSeat(42);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[42].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[42] = "2";
				setYellowSeat(42);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(43).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[43].equals("2")){
					setGreenSeat(43);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[43].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[43] = "2";
				setYellowSeat(43);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(44).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[44].equals("2")){
					setGreenSeat(44);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[44].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[44] = "2";
				setYellowSeat(44);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(45).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[45].equals("2")){
					setGreenSeat(45);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[45].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[45] = "2";
				setYellowSeat(45);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(46).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[46].equals("2")){
					setGreenSeat(46);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[46].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[46] = "2";
				setYellowSeat(46);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatNormal.get(47).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefNormal()[47].equals("2")){
					setGreenSeat(47);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefNormal()[47].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefNormal()[47] = "2";
				setYellowSeat(47);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		
	}
	public void setYellowSeat(int index){
		ClassLoader loader = this.getClass().getClassLoader();
		label_seatNormal.get(index).setIcon(new ImageIcon(loader.getResource("images/seat_yellow.png")));
		bookedNO.add("N" + (index+1));
	}
	public void setGreenSeat(int index){
		ClassLoader loader = this.getClass().getClassLoader();
		label_seatNormal.get(index).setIcon(new ImageIcon(loader.getResource("images/seat_green.png")));
		Database.getSeatLayoutRefNormal()[index]="0";
		for(int i =0;i<bookedNO.size();i++){
			if(bookedNO.get(i).equals("N" +(index+1)))
				bookedNO.remove(i);
		}
	}
	public void checkBooked(){
		
	}
}
