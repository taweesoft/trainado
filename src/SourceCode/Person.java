package SourceCode;

import java.util.ArrayList;


public class Person {
	private String ID,name,lastname,gender,email,telephone;
	protected static ArrayList<Customer> customerList = new ArrayList<Customer>();
	public Person(){
		
	}
	public Person(String ID,String name,String lastname,String gender,String email,String telephone){
		this.ID = ID;
		this.name = name;
		this.lastname = lastname;
		this.gender = gender;
		this.email = email;
		this.telephone = telephone;
		
	}
	public static ArrayList<Customer> getCustomerList(){
		return customerList;
	}
//	public boolean equals(Person key){
//		if(	ID.equals(key.ID) && 
//			name.equals(key.name) && 
//			lastname.equals(key.lastname) && 
//			gender.equals(key.gender)){
//			return true;
//		}
//		return false;
//	}
	public String toString(){
		return String.format("%s,%s,%s",name,lastname,gender);
	}
	
	//Mutator
	public void setID(String ID){
		this.ID = ID;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setLastname(String lastname){
		this.lastname = lastname;
	}
	public void setGender(String gender){
		this.gender = gender;
	}
	
	//Accessor
	public String getID(){
		return ID;
	}
	public String getName(){
		return name;
	}
	public String getLastname(){
		return lastname;
	}
	public String getGender(){
		return gender;
	}
	public String getEmail(){
		return email;
	}
	public String getTelephone(){
		return telephone;
	}
}
