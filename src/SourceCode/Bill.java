package SourceCode;


public class Bill {
	private Customer payer;
	private Ticket ticket;
	private int billID;
	private String flightInfo;
	private String seatNO;
	private double price;
	static private int numberOfBills=1;;
	
	public Bill(Customer payer,Ticket ticket){
		this.payer = payer;
		this.ticket = ticket;
		this.price = ticket.getPrice();
		billID = numberOfBills;
		this.flightInfo = ticket.getFlightInfo().getInfo();
		this.seatNO = ticket.getSeatNO();
		numberOfBills++;
	}
	public Bill(int billID,Customer payer,String flightInfo,String seatNO,double price){
		this.billID = billID;
		this.payer = payer;
		this.price = price;
		this.flightInfo = flightInfo;
		this.seatNO = seatNO;
		numberOfBills++;
	}
	public String toString(){
		return String.format("BillID:%d,%s,%s,%s,%s,%.2f", this.billID, payer.getName(),payer.getLastname(),this.flightInfo,this.seatNO, this.price);
	}
	
	public Customer getPayer(){
		return payer;
	}
	public Ticket getTicket(){
		return ticket;
	}
	public int getBillID(){
		return billID;
	}
	public int getNumberOfBills(){
		return numberOfBills;
	}
}
