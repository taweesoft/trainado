package SourceCode;

import java.util.ArrayList;
import java.util.Arrays;


public class Flight {
	private int[] numSeats;
	private int[] numBookings;
	private FlightInfo flightInfo;
	private double flightExpense;
	private ArrayList<Customer> passengerList;
	private double[] travelClassPrice;
	
	
	public Flight(int[] numSeats,FlightInfo flightInfo,double flightExpense,double[] travelClassPrice){
		passengerList = new ArrayList<Customer>();
		numBookings = new int[3];
		this.numSeats = numSeats;
		this.flightInfo = flightInfo;
		this.flightExpense = flightExpense;
		this.travelClassPrice = travelClassPrice;
	}
	public double getPrice_PreBooking(String travelClass,int memberClass){
		double price=-1;
		if(travelClass.equals("Normal")){
			price = travelClassPrice[0];
		}else if(travelClass.equals("Business")){
			price = travelClassPrice[1];
		}else if(travelClass.equals("Premium")){
			price = travelClassPrice[2];
		}
		switch(memberClass){
		case 1:
			price -= price*0.10;
			break;
		case 2:
			price -= price*0.20;
			break;
		}
		return price;
	}
	public int booking(Customer passenger,String travelClass,String seatNO){
		double price=-1;
		int index=-1;
		if(travelClass.equals("Normal")){
			price = travelClassPrice[0];
			index =0;
		}else if(travelClass.equals("Business")){
			price = travelClassPrice[1];
			index =1;
		}else if(travelClass.equals("Premium")){
			price = travelClassPrice[2];
			index =2;
		}
		if(numBookings[index]< numSeats[index]){
			passengerList.add(passenger);
			numBookings[index]++;
			Ticket ticket = new Ticket(passenger,flightInfo,travelClass,price,seatNO,"Charged",1);
			passenger.getTicketList().add(ticket);
			String ticketInfo = passenger.getName()+","+
								passenger.getLastname()+","+
								ticket.toString();
			Database.getTicketArr().add(ticketInfo.split(","));
			Database.saveTicketList();
			Database.getRecentArr().add(ticketInfo.split(","));
			Database.saveRecent();
			return 1;
		}
		return -1;
	}
	public int[] getNumSeats() {
		return numSeats;
	}
	public int[] getNumBookings() {
		return numBookings;
	}
	public FlightInfo getFlightInfo() {
		return flightInfo;
	}
	public double getFlightExpense() {
		return flightExpense;
	}
	public ArrayList<Customer> getPassengerList() {
		return passengerList;
	}
	public double[] getTravelClassPrice() {
		return travelClassPrice;
	}
	public int searchPassenger(Customer passenger){
		for(int i =0;i<passengerList.size();i++){
			if(passengerList.get(i).equals(passenger))
				return i;
		}
		return -1;
	}
	public int cancel(Customer passenger){
		if(searchPassenger(passenger) != -1){
			for(int i =0;i<passenger.getTicketList().size();i++){
				if(passenger.getTicketList().get(i).getFlightInfo().equals(this.flightInfo)){
					String travelClass=passenger.getTicketList().get(i).getTravelClass();
					int index=-1;
					if(travelClass.equals("N"))
						index=0;
					else if(travelClass.equals("B"))
						index=1;
					else if(travelClass.equals("P"))
						index=2;
					passenger.getTicketList().remove(i);
					numBookings[index]--;
				}
			}
			passengerList.remove(searchPassenger(passenger));
			return 1;
		}
		return -1;
	}
	public void clear(){
		for(int i=0;i<passengerList.size();i++){
			System.out.println(passengerList.size());
			if(passengerList.get(i).searchTicket(this.flightInfo) != -1){
				passengerList.get(i).getTicketList().remove(passengerList.get(i).searchTicket(this.flightInfo));
			}
		}
		passengerList.clear();
		numBookings[0] =0;
		numBookings[1] =0;
		numBookings[2] =0;
		
		for(int i=0;i<Database.getSeatLayoutRefPremium().length;i++){
			if(Database.getSeatLayoutRefPremium()[i].equals("1"))
				Database.getSeatLayoutRefPremium()[i] = "0";
		}
		for(int i=0;i<Database.getSeatLayoutRefBusiness().length;i++){
			if(Database.getSeatLayoutRefBusiness()[i].equals("1"))
				Database.getSeatLayoutRefBusiness()[i] = "0";
		}
		for(int i=0;i<Database.getSeatLayoutRefNormal().length;i++){
			if(Database.getSeatLayoutRefNormal()[i].equals("1"))
				Database.getSeatLayoutRefNormal()[i] = "0";
		}
		Database.saveSeatLayoutSQL();
	}
	public String toString() {
		String temp = "";
		temp += flightInfo.toString() + "\n";
		if (numBookings[0] > 0 || numBookings[1] > 0 ||numBookings[2] > 0) {
			for ( int i = 0; i < passengerList.size(); i++ ) {
				temp += String.format("%d, %s\n", i+1, passengerList.get(i).toString());
			}
		} else {
			temp += "No booking\n";
		}
			return temp;
	}
	
	
	
	
}
