package SourceCode;


public class RoundInfo {
	private String info;
	private Time fromTime,toTime;
	public RoundInfo(){
		
	}
	public RoundInfo(String info, Time fromTime, Time toTime){
		this.info = info;
		this.fromTime = fromTime;
		this.toTime = toTime;
	}
	public boolean equals(RoundInfo key){
		if(this.info.equals(key.info) &&
			this.fromTime.getDuration()==key.fromTime.getDuration() &&
			this.toTime.getDuration()==key.toTime.getDuration()){
			return true;
		}
		return false;
	}
	public String toString(){
		return String.format("%s, %s, %s",info,fromTime.toString(),toTime.toString());
	}
	
	//Mutator
	public void setInfo(String info){
		this.info = info;
	}
	public void setFromtime(Time fromTime){
		this.fromTime = fromTime;
	}
	public void setTotime(Time toTime){
		this.toTime = toTime;
	}
	
	//Accessor
	public String getInfo(){
		return info;
	}
	public Time getFromtime(){
		return fromTime;
	}
	public Time getTotime(){
		return toTime;
	}
}
