package SourceCode;


public class Ticket {
	private Customer passenger;
	private FlightInfo flightInfo;
	private double price;
	private String paymentStatus;
	private String travelClass;
	private String seatNO;
	public Ticket(Customer passenger,FlightInfo flightInfo,String travelClass,double price,String seatNO,String status,int ApplyOrNot){
		this.passenger = passenger;
		this.flightInfo = flightInfo;
		this.travelClass = travelClass;
		this.price = price;
		this.seatNO = seatNO;
		this.paymentStatus = status ;
		if(ApplyOrNot==1){
			applyDiscount();
		}
	}
	public void applyDiscount(){
		this.price = this.price - (this.price * passenger.getDiscountRate()/100);
	}
	public void setPaymentStatus(String newStatus){
		this.paymentStatus = newStatus;
	}
	public boolean equals(Ticket key){
		return this.flightInfo.equals(key.flightInfo) && this.passenger.equals(key.passenger);
	}
	public String toString(){
		return String.format("%s,%s,%s,%.2f,%s",flightInfo.toString(),travelClass,seatNO, price,paymentStatus);
	}
	public Customer getPassenger(){
		return passenger;
	}
	public FlightInfo getFlightInfo(){
		return flightInfo;
	}
	public double getPrice(){
		return price;
	}
	public String getPaymentStatus(){
		return paymentStatus;
	}
	public String getTravelClass(){
		return travelClass;
	}
	public String getSeatNO(){
		return seatNO;
	}
	
}
