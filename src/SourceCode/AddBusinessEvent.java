package SourceCode;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import GUI.Train;


public class AddBusinessEvent {
	private ArrayList<JLabel> label_seatBusiness = new ArrayList<JLabel>();
	private ArrayList<String> bookedNO = new ArrayList<String>();
	public AddBusinessEvent(ArrayList<JLabel> label_seatBusiness,final Train train,ArrayList<String> bookedNO){
		this.label_seatBusiness = label_seatBusiness;
		this.bookedNO = bookedNO;
		label_seatBusiness.get(0).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[0].equals("2")){
					setGreenSeat(0);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else
			if(!Database.getSeatLayoutRefBusiness()[0].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[0] = "2";
				setYellowSeat(0);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(1).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[1].equals("2")){
					setGreenSeat(1);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[1].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[1] = "2";
				setYellowSeat(1);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(2).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[2].equals("2")){
					setGreenSeat(2);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[2].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[2] = "2";
				setYellowSeat(2);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(3).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[3].equals("2")){
					setGreenSeat(3);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[3].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[3] = "2";
				setYellowSeat(3);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(4).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[4].equals("2")){
					setGreenSeat(4);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[4].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[4] = "2";
				setYellowSeat(4);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(5).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[5].equals("2")){
					setGreenSeat(5);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[5].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[5] = "2";
				setYellowSeat(5);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(6).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[6].equals("2")){
					setGreenSeat(6);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[6].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[6] = "2";
				setYellowSeat(6);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(7).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[7].equals("2")){
					setGreenSeat(7);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[7].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[7] = "2";
				setYellowSeat(7);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(8).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[8].equals("2")){
					setGreenSeat(8);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[8].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[8] = "2";
				setYellowSeat(8);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(9).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[9].equals("2")){
					setGreenSeat(9);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[9].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[9] = "2";
				setYellowSeat(9);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(10).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[10].equals("2")){
					setGreenSeat(10);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[10].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[10] = "2";
				setYellowSeat(10);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(11).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[11].equals("2")){
					setGreenSeat(11);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[11].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[11] = "2";
				setYellowSeat(11);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(12).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[12].equals("2")){
					setGreenSeat(12);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[12].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[12] = "2";
				setYellowSeat(12);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(13).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[13].equals("2")){
					setGreenSeat(13);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[13].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[13] = "2";
				setYellowSeat(13);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(14).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[14].equals("2")){
					setGreenSeat(14);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[14].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[14] = "2";
				setYellowSeat(14);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(15).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[15].equals("2")){
					setGreenSeat(15);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[15].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[15] = "2";
				setYellowSeat(15);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(16).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[16].equals("2")){
					setGreenSeat(16);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[16].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[16] = "2";
				setYellowSeat(16);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(17).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[17].equals("2")){
					setGreenSeat(17);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[17].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[17] = "2";
				setYellowSeat(17);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(18).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[18].equals("2")){
					setGreenSeat(18);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[18].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[18] = "2";
				setYellowSeat(18);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(19).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[19].equals("2")){
					setGreenSeat(19);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[19].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[19] = "2";
				setYellowSeat(19);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(20).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[20].equals("2")){
					setGreenSeat(20);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[20].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[20] = "2";
				setYellowSeat(20);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(21).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[21].equals("2")){
					setGreenSeat(21);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[21].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[21] = "2";
				setYellowSeat(21);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(22).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[22].equals("2")){
					setGreenSeat(22);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[22].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[22] = "2";
				setYellowSeat(22);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(23).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[23].equals("2")){
					setGreenSeat(23);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[23].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[23] = "2";
				setYellowSeat(23);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(24).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[24].equals("2")){
					setGreenSeat(24);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[24].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[24] = "2";
				setYellowSeat(24);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(25).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[25].equals("2")){
					setGreenSeat(25);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[25].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[25] = "2";
				setYellowSeat(25);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(26).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[26].equals("2")){
					setGreenSeat(26);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[26].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[26] = "2";
				setYellowSeat(26);
				
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(27).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[27].equals("2")){
					setGreenSeat(27);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[27].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[27] = "2";
				setYellowSeat(27);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(28).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[28].equals("2")){
					setGreenSeat(28);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[28].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[28] = "2";
				setYellowSeat(28);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(29).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[29].equals("2")){
					setGreenSeat(29);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[29].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[29] = "2";
				setYellowSeat(29);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(30).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[30].equals("2")){
					setGreenSeat(30);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[30].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[30] = "2";
				setYellowSeat(30);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
		label_seatBusiness.get(31).addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){
				if(Database.getSeatLayoutRefBusiness()[31].equals("2")){
					setGreenSeat(31);
					train.updateBookedSeat(2);
				
					train.updateBookedSeat(3);
				}else 
			if(!Database.getSeatLayoutRefBusiness()[31].equals("1") && train.updateBookedSeat(1)==1){
				Database.getSeatLayoutRefBusiness()[31] = "2";
				setYellowSeat(31);
				train.updateBookedSeat(2);
			}
		}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){}
		});
	}
	public void setYellowSeat(int index){
		ClassLoader loader = this.getClass().getClassLoader();
		label_seatBusiness.get(index).setIcon(new ImageIcon(loader.getResource("images/seat_yellow.png")));
		bookedNO.add("B" + (index+1));
	}
	public void setGreenSeat(int index){
		ClassLoader loader = this.getClass().getClassLoader();
		label_seatBusiness.get(index).setIcon(new ImageIcon(loader.getResource("images/seat_green.png")));
		Database.getSeatLayoutRefBusiness()[index]="0";
		for(int i =0;i<bookedNO.size();i++){
			if(bookedNO.get(i).equals("B" +(index+1)))
				bookedNO.remove(i);
		}
	}
}
