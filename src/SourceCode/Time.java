package SourceCode;


public class Time {
	private int hour,minute,second;
	public Time(int hour,int minute,int second){
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}
	public Time(int duration){
		hour = duration/3600;
		duration %=3600;
		minute = duration/60;
		second = duration%60;
	}
	public int getDuration(){
		return (hour*3600)+(minute*60)+second;
	}
	public Time add(Time other){
		int hour = this.hour + other.hour;
		int minute = this.minute + other.minute;
		int second = this.second + other.second;
		
		minute += second/60;
		second %=60;
		hour += minute/60;
		minute %=60;
		return new Time(hour,minute,second);
	}
	public int subtract(Time other){
		if(this.getDuration()<other.getDuration()){
			return ((86400+this.getDuration())-other.getDuration());
		}
		return this.getDuration() - other.getDuration();
	}
	public boolean equals(Time key){
		if(this.getDuration()==key.getDuration()){
			return true;
		}
		return false;
	}
	public String toString(){
		return String.format("%02d:%02d",hour,minute);
	}
	
	//Mutator
	public void setHour(int hour){
		this.hour = hour;
	}
	public void setMinute(int minute){
		this.minute = minute;
	}
	public void setSecond(int second){
		this.second = second;
	}
	
	//Accessor
	public int getHour(){
		return hour;
	}
	public int getMinute(){
		return minute;
	}
	public int getSecond(){
		return second;
	}
	
}
