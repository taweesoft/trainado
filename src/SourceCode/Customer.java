package SourceCode;

import java.util.ArrayList;

public class Customer extends Person{
	private String customerID;
	private int memberClass;
	private double discountRate;
	private ArrayList<Ticket> ticketList;
	private ArrayList<Bill> billList;
	static private int numberOfCustomers=1;
	
	
	public String getCustomerID() {
		return customerID;
	}
	public int getMemberClass() {
		return memberClass;
	}
	public double getDiscountRate() {
		return discountRate;
	}
	public ArrayList<Ticket> getTicketList() {
		return ticketList;
	}
	public ArrayList<Bill> getBillList() {
		return billList;
	}
	public static int getNumberOfCustomers() {
		return numberOfCustomers;
	}
	public Customer(String ID,String name,String lastname,String telephone,String email,String gender,int memberClass){
		super(ID,name,lastname,gender,email,telephone);
		this.memberClass = memberClass;
		setDiscountRate(this.memberClass);
		ticketList = new ArrayList<Ticket>();
		billList = new ArrayList<Bill>();
		this.customerID = ID;
		customerList.add(this);
		numberOfCustomers++;
	}
	public void addTicket(Ticket ticket){
		ticketList.add(ticket);
	}
	public int searchTicket(FlightInfo key){
		for(int i =0 ; i<ticketList.size();i++){
			if(ticketList.get(i).getFlightInfo().equals(key))
				return i;
		}
		return -1;
	}
	public void addBill(Bill bill){
		billList.add(bill);
	}
	public void setDiscountRate(int memberClass){
		switch(memberClass){
		case 1:
			discountRate = 10;
			break;
		case 2:
			discountRate = 20;
			break;
		default: 
			discountRate =0;
		}
	}
	public void setMemberClass(int memberClass){
		this.memberClass = memberClass;
		setDiscountRate(memberClass);
	}
	public boolean equals(String firstname,String lastname){
		return this.getName().equals(firstname) && this.getLastname().equals(lastname);
	}
	public boolean equals(Customer key){
		return this.getName().equals(key.getName()) && this.getLastname().equals(key.getLastname()) && this.getTelephone().equals(key.getTelephone());
	}
	public String toString(){
		return String.format("%s,%s,%s,%s,%s,%s,%d",this.customerID,this.getName(),this.getLastname(),this.getTelephone(),this.getEmail(),this.getGender(),this.memberClass);
	}
	public String getTicketListToString(){
		String ticketInfo="";	
		for(int i =0;i<ticketList.size();i++){
			ticketInfo += ticketList.get(i).toString() +"\n";
		}
		return ticketInfo;
	}
	public String getBillListToString(){
		String billInfo="";	
		for(int i =0;i<billList.size();i++){
			billInfo += billList.get(i).toString() +"\n";
		}
		return billInfo;
	}
	public void removeSpecificTicket(String flightinfo,String seatNO){
		for(int i=0;i<ticketList.size();i++){
			if(ticketList.get(i).getFlightInfo().getInfo().equals(flightinfo) && 
					ticketList.get(i).getSeatNO().equals(seatNO)){
				ticketList.remove(i);
			}
		}
	}
	public String getBillInformation(){
		return this.getName() + "," + this.getLastname() + "," + billList.get(billList.size()-1).toString();
	}
}
