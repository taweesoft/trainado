package SourceCode;


public class Store {
	private static double revenue;
	private static double expense;
	public Store(){
	}
	public Store(double revenue,double expense){
		this.revenue = revenue;
		this.expense = expense;
	}
	public int booking(Flight flight,Customer passenger,String travelClass,String seatNO){
		return flight.booking(passenger, travelClass,seatNO);
	}
	public int cancel(Flight flight,Customer passenger){
		return flight.cancel(passenger);
	}
	public static int receivePayment(Flight flight,Customer passenger){
		if(flight.searchPassenger(passenger) != -1 || passenger.searchTicket(flight.getFlightInfo()) != -1){
			for(int i=0;i<passenger.getTicketList().size();i++){
				if(passenger.getTicketList().get(i).getFlightInfo().equals(flight.getFlightInfo())){
					revenue += passenger.getTicketList().get(i).getPrice();
					changeInTicketArr(flight,passenger);
					passenger.getTicketList().get(i).setPaymentStatus("Paid");
					Bill bill = new Bill(passenger,passenger.getTicketList().get(i));
					passenger.getBillList().add(bill);
					String billStr = passenger.getBillInformation();
					Database.getBillArr().add(billStr.split(","));
					String ticketInfo = passenger.getName() +"," + 
							passenger.getLastname() + ","+
							passenger.getTicketList().get(i).toString();
					Database.getRecentArr().add(ticketInfo.split(","));
				}
			}
			Database.saveTicketList();
			Database.saveBillList();
			Database.saveRecent();
			return 1;
		}
		return -1;
	}
	public static void changeInTicketArr(Flight flight,Customer passenger){
		for(int k=0;k<Database.getTicketArr().size();k++){
			if(passenger.getName().equals(Database.getTicketArr().get(k)[0]) &&
					passenger.getLastname().equals(Database.getTicketArr().get(k)[1]) &&
					Database.getTicketArr().get(k)[2].equals(flight.getFlightInfo().getInfo())){
				Database.getTicketArr().get(k)[9] = "Paid";
			}
		}
	}
	public static void changeInTicketArr(Flight flight,Customer passenger,String seatNO){
		for(int k=0;k<Database.getTicketArr().size();k++){
			if(passenger.getName().equals(Database.getTicketArr().get(k)[0]) &&
					passenger.getLastname().equals(Database.getTicketArr().get(k)[1]) &&
					Database.getTicketArr().get(k)[2].equals(flight.getFlightInfo().getInfo()) &&
					Database.getTicketArr().get(k)[7].equals(seatNO)){
				Database.getTicketArr().get(k)[9] = "Paid";
			}
		}
	}
	public static int receivePayment(Flight flight,Customer passenger,String seatNO){
		if(flight.searchPassenger(passenger) != -1 || passenger.searchTicket(flight.getFlightInfo()) != -1){
			for(int i=0;i<passenger.getTicketList().size();i++){
				if(passenger.getTicketList().get(i).getFlightInfo().equals(flight.getFlightInfo()) && 
						passenger.getTicketList().get(i).getSeatNO().equals(seatNO)){
					revenue += passenger.getTicketList().get(i).getPrice();
					changeInTicketArr(flight,passenger,seatNO);
					passenger.getTicketList().get(i).setPaymentStatus("Paid");
					Bill bill = new Bill(passenger,passenger.getTicketList().get(i));
					passenger.getBillList().add(bill);
					String billStr = passenger.getBillInformation();
					Database.getBillArr().add(billStr.split(","));
				}
			}
			Database.saveTicketList();
			Database.saveBillList();
			return 1;
		}
		return -1;
	}
	public void clearFlight(Flight flight){
		this.expense += flight.getFlightExpense();
		flight.clear();
	}
	public double getProfit(){
		return revenue-expense;
	}
	public String toString(){
		return String.format("Revenue: %.2f, Expense: %.2f", revenue,expense);
	}
	public static double getRevenue() {
		return revenue;
	}
	public static double getExpense() {
		return expense;
	}
	public static void setRevenue(double revenue){
		Store.revenue = revenue;
	}
	public static void setExpense(double expense){
		Store.expense = expense;
	}
	
}
