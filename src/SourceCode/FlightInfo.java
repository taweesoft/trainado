package SourceCode;


public class FlightInfo extends RoundInfo{
	private String fromCity,toCity;
	public FlightInfo(){
		
	}
	public FlightInfo(String info,Time fromTime,Time toTime,String fromCity,String toCity){
		super(info,fromTime,toTime);
		this.fromCity = fromCity;
		this.toCity = toCity;
	}
	public boolean equals(FlightInfo key){
		return (key.getInfo().equals(this.getInfo()) && 
				key.getFromtime().equals(this.getFromtime()) && 
				key.getTotime().equals(this.getTotime()) &&
				key.getFromCity().equals(this.getFromCity()) && 
				key.getToCity().equals(this.getToCity())
				);
			
		}
		
	
	public String toString(){
		return String.format("%s,%s,%s,%s", this.getInfo(),this.getFromCity(),this.getToCity(),this.getFromtime().toString()+ "-" +this.getTotime().toString());
	}
	public String getFromCity(){
		return fromCity;
	}
	public String getToCity(){
		return toCity;
	}

}
