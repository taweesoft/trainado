package SourceCode;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JOptionPane;


public class Database {
	private static Connection connect;
	private static String url = "pcw.dyndns.tv:3306/";
	private static String DBname = "Trainado";
	private static String TBname = "Seat_layout";
	private static String username = "Trainado";
	private static String password = "Trainado";
	
	private static String[] seatLayoutRefPremium;
	private static String[] seatLayoutRefBusiness;
	private static String[] seatLayoutRefNormal;
	private static ArrayList<String[]> loginArr = new ArrayList<String[]>();
	private static ArrayList<String[]> ticketArr = new ArrayList<String[]>();
	private static ArrayList<String[]> billArr = new ArrayList<String[]>();
	private static ArrayList<String[]> adminArr = new ArrayList<String[]>();
	private static ArrayList<String[]> allSeat = new ArrayList<String[]>();
	private static ArrayList<String[]> recentArr = new ArrayList<String[]>();
	private static String loginStr = "/Trainado_login.txt";
	private static String layoutStr = "/Trainado_seat_layout.txt";
	private static String ticketStr = "/Trainado_ticket.txt";
	private static String billStr = "/Trainado_bill.txt";
	private static String adminStr = "/Trainado_admin.txt";
	private static String recentStr = "/Trainado_recent.txt";
	public static int flightIndex;
	
	public static ArrayList<String[]> getLoginArr(){
		return loginArr;
	}
	public static ArrayList<String[]> getTicketArr(){
		return ticketArr;
	}
	public static ArrayList<String[]> getBillArr(){
		return billArr;
	}
	public static ArrayList<String[]> getAdminArr(){
		return adminArr;
	}
	public static ArrayList<String[]> getAllSeat(){
		return allSeat;
	}
	public static ArrayList<String[]> getRecentArr(){
		return recentArr;
	}
	public static String[] getSeatLayoutRefPremium(){
		return seatLayoutRefPremium;
	}
	public static String[] getSeatLayoutRefBusiness(){
		return seatLayoutRefBusiness;
	}
	public static String[] getSeatLayoutRefNormal(){
		return seatLayoutRefNormal;
	}
	public static int getFlightIndex(){
		return flightIndex;
	}
	public Database(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://"+url+DBname,username,password);
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Can't connect to sql database \n Switch to use text file database");
			e.printStackTrace();
		}
		getAdminLogin();
		getLogin();
		getTicketList();
		createCustomer();
		createTicket();
		createPassengerList();
		getBillList();
		createBill();
		setFinance();
	}
	
	public static void getRecent(){
		File file = new File(recentStr);
		try{
			if(!file.exists())
				file.createNewFile();
			Scanner scan = new Scanner(file);
			while(scan.hasNext()){
				recentArr.add(scan.nextLine().split(","));
			}
			scan.close();
		}catch(Exception e){}
		
	}
	public static void saveRecent(){
		try{
			BufferedWriter buffer = new BufferedWriter(new FileWriter(recentStr));
			for(int i=0;i<recentArr.size();i++){
				String recent = NoBracketNoSpace(Arrays.toString(recentArr.get(i)));
				buffer.write(recent);
				buffer.newLine();
			}
			buffer.close();
		}catch(Exception e){}
	}
	public static void getAllSeatLayoutSQL(){
		try{
			Statement statement = connect.createStatement();
			ResultSet result = statement.executeQuery("Select * from "+ TBname);
			allSeat.clear();
			while(result.next()){
				allSeat.add(result.getString(2).split(","));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public static void saveInitialSeatLayout(){
		try{
			BufferedWriter buffer = new BufferedWriter(new FileWriter(layoutStr));
			for(int i=0;i<allSeat.size();i++){
				buffer.write(NoBracketNoSpace(Arrays.toString(allSeat.get(i))));
				buffer.newLine();
			}
			buffer.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void getSeatLayoutSQL(){
		try{
			Statement c = connect.createStatement();
			ResultSet result = c.executeQuery("Select * from "+ TBname);
			int count=0;
			while(result.next()){
				if(count==flightIndex*3-3){
					seatLayoutRefPremium = result.getString(2).split(",");
				}else if(count==flightIndex*3-2)
					seatLayoutRefBusiness = result.getString(2).split(",");
				else if(count==flightIndex*3-1){
					seatLayoutRefNormal = result.getString(2).split(",");
					break;
				}else{
					String[] temp = result.getString(2).split(",");
				}
				count++;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void saveSeatLayoutSQL(){
		try{
			Statement statement = connect.createStatement();
			String premiumString = NoBracketNoSpace(Arrays.toString(seatLayoutRefPremium));
			String premiumChange2To1 = premiumString.replace("2", "1");
			String sqlPremium = "UPDATE " + TBname + " SET layout = '" + premiumChange2To1 +"' WHERE ID ='" + Integer.toString(flightIndex*3-2) +"'";
			statement.execute(sqlPremium);
			
			String businessString = NoBracketNoSpace(Arrays.toString(seatLayoutRefBusiness));
			String businessChange2To1 = businessString.replace("2", "1");
			String sqlBusiness = "UPDATE " + TBname + " SET layout = '" + businessChange2To1 +"' WHERE ID ='" + Integer.toString(flightIndex*3-1) +"'";
			statement.execute(sqlBusiness);
			
			String normalString = NoBracketNoSpace(Arrays.toString(seatLayoutRefNormal));
			String normalChange2To1 = normalString.replace("2", "1");
			String sqlNormal = "UPDATE " + TBname + " SET layout = '" + normalChange2To1 +"' WHERE ID ='" + Integer.toString(flightIndex*3) +"'";
			statement.execute(sqlNormal);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		saveSeatLayout();
	}
	public static void getAdminLogin(){
		File file = new File(adminStr);
		try{
			if(!file.exists()){
				file.createNewFile();
				BufferedWriter buffer  = new BufferedWriter(new FileWriter(adminStr));
				buffer.write("1,Taweesoft,taweesoft,Taweerat,Chaiman");
				buffer.newLine();
				buffer.write("2,Nott,nott,Patinya,Yongyai");
				buffer.newLine();
				buffer.close();
			}
			Scanner scan = new Scanner(file);
			while(scan.hasNext()){
				adminArr.add(scan.nextLine().split(","));
			}
			scan.close();
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "Permission denied for create new file\nTry to run program in another directory","Permission denied",JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	public static void getLogin(){
		File file = new File(loginStr);
			try{
				if(!file.exists()){
					file.createNewFile();
				}
				Scanner scan = new Scanner(file);
				loginArr.clear();
					while(scan.hasNext()){
						loginArr.add(scan.nextLine().split(","));
					}
				scan.close();
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	public static void saveLogin(){
		try{
			BufferedWriter filewriter = new BufferedWriter(new FileWriter(loginStr));
			for(int i=0;i<loginArr.size();i++){
				String loginString = NoBracketNoSpace(Arrays.toString(loginArr.get(i)));
				filewriter.write(loginString);
				filewriter.newLine();
			}
			filewriter.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void getSeatLayout(){
		File file = new File(layoutStr);
		int count=0;
			try{
				if(!file.exists()){
					file.createNewFile();
					getAllSeatLayoutSQL();
					BufferedWriter buffer = new BufferedWriter(new FileWriter(layoutStr));
					for(int i=0;i<allSeat.size();i++){
						String allSeatString = NoBracketNoSpace(Arrays.toString(allSeat.get(i)));
						buffer.write(allSeatString);
						buffer.newLine();
					}
					buffer.close();
				}
				Scanner scan = new Scanner(file);
				while(scan.hasNext()){
					if(count==flightIndex*3-3){
						seatLayoutRefPremium = scan.nextLine().split(",");
					}else if(count==flightIndex*3-2)
						seatLayoutRefBusiness = scan.nextLine().split(",");
					else if(count==flightIndex*3-1){
						seatLayoutRefNormal = scan.nextLine().split(",");
						break;
					}else{
						String[] temp = scan.nextLine().split(",");
					}
					count++;
				}
				scan.close();
				saveSeatLayoutSQL();
			}catch(Exception e){
				e.printStackTrace();
			}
			initialNumBooking();
		
	}
	public static void initialNumBooking(){
		
		for(int i=0;i<seatLayoutRefPremium.length;i++){
			if(seatLayoutRefPremium[i].equals("1")){
				CreateFlight.flight[flightIndex-1].getNumBookings()[2]++;
			}
		}
		for(int i=0;i<seatLayoutRefBusiness.length;i++){
			if(seatLayoutRefBusiness[i].equals("1")){
				CreateFlight.flight[flightIndex-1].getNumBookings()[1]++;
			}
		}
		for(int i=0;i<seatLayoutRefNormal.length;i++){
			if(seatLayoutRefNormal[i].equals("1")){
				CreateFlight.flight[flightIndex-1].getNumBookings()[0]++;
			}
		}
	}
	public static void saveSeatLayout(){
		ArrayList<String[]> layout = new ArrayList<String[]>();
		try{
			File file = new File(layoutStr);
			if(file.exists()){
				Scanner scan = new Scanner(file);
				while(scan.hasNext()){
					layout.add(scan.nextLine().split(","));
				}
				scan.close();
			}else{
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			for(int i=0;i<seatLayoutRefPremium.length;i++) layout.get(flightIndex*3-3)[i] = seatLayoutRefPremium[i];
			for(int i=0;i<seatLayoutRefBusiness.length;i++) layout.get(flightIndex*3-2)[i] = seatLayoutRefBusiness[i];
			for(int i=0;i<seatLayoutRefNormal.length;i++) layout.get(flightIndex*3-1)[i] = seatLayoutRefNormal[i];
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(layoutStr));
			for(int i=0;i<layout.size();i++){
				String layoutString = NoBracketNoSpace( Arrays.toString(layout.get(i)));
				String change2To1 = layoutString.replace("2", "1");
				bw.write(change2To1);
				bw.newLine();
			}
			bw.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void getTicketList(){
		File file = new File(ticketStr);
			try{
				if(!file.exists())
					file.createNewFile();
				Scanner scan = new Scanner(file);
				ticketArr.clear();
				while(scan.hasNext()){
					ticketArr.add(scan.nextLine().split(","));
				}
				scan.close();
			}catch(Exception e){}
		
	}
	public static void saveTicketList(){
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(ticketStr));
			for(int i=0;i<ticketArr.size();i++){
				String ticketNoSpace = Arrays.toString(ticketArr.get(i)).substring(1,Arrays.toString(ticketArr.get(i)).length()-1).replace(" ", "");
				bw.write(ticketNoSpace);
				bw.newLine();
			}
			bw.close();
		}catch(Exception e){}
	}
	
	public void getBillList(){
		File file = new File(billStr);
			try{
				if(!file.exists())
					file.createNewFile();
				Scanner scan = new Scanner(file);
				while(scan.hasNext()){
					billArr.add(scan.nextLine().split(","));
				}
				scan.close();
			}catch(Exception e){}
	}
	public static void saveBillList(){
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(billStr));
			for(int i=0;i<billArr.size();i++){
				String bill = Arrays.toString(billArr.get(i));
				bw.write(NoBracketNoSpace(bill));
				bw.newLine();
			}
			bw.close();
		}catch(Exception e){}
	}
	public static void createCustomer(){
		for(int i=0;i<loginArr.size();i++){
				String ID = loginArr.get(i)[0];
				String Firstname = loginArr.get(i)[3];
				String Lastname = loginArr.get(i)[4];
				String tel = loginArr.get(i)[5];
				String email = loginArr.get(i)[6];
				String gender = loginArr.get(i)[7];
				int memberClass = Integer.parseInt(loginArr.get(i)[8]);
				Customer customer = new Customer(ID,Firstname,Lastname,tel,email,gender,memberClass);
			}
	}
	public static void createTicket(){
		for(int i=0;i<Person.customerList.size();i++){
			for(int k=0;k<ticketArr.size();k++){
				if(Person.customerList.get(i).getName().equals(ticketArr.get(k)[0]) && 
						Person.customerList.get(i).getLastname().equals(ticketArr.get(k)[1])){
					Customer passenger = Person.customerList.get(i);
					FlightInfo flightInfo = CreateFlight.flightInfo[CreateFlight.searchFlightInfo(ticketArr.get(k)[2])];
					String travelClass = ticketArr.get(k)[6];
					double price = Double.parseDouble(ticketArr.get(k)[8]);
					String seatNO = ticketArr.get(k)[7];
					String status = ticketArr.get(k)[9];
					Person.customerList.get(i).addTicket(new Ticket(passenger,flightInfo,travelClass,price,seatNO,status,2));
				}
			}
		}
	}
	public static void createPassengerList(){
		for(int i=0;i<CreateFlight.flight.length;i++){
			for(int k=0;k<Person.customerList.size();k++){
				for(int j=0;j<Person.customerList.get(k).getTicketList().size();j++){
					if(Person.customerList.get(k).getTicketList().get(j).getFlightInfo().equals(CreateFlight.flight[i].getFlightInfo())){
						CreateFlight.flight[i].getPassengerList().add(Person.customerList.get(k));
					}
				}
				
			}
		}
	}
	public void createBill(){
		for(int i=0;i<Person.customerList.size();i++){
			for(int k=0;k<billArr.size();k++){
				if(Person.customerList.get(i).equals(billArr.get(k)[0], billArr.get(k)[1])){
					Bill bill = new Bill(Integer.parseInt(billArr.get(k)[2].substring(7)),Person.customerList.get(i),billArr.get(k)[5],billArr.get(k)[6],Double.parseDouble(billArr.get(k)[7]));
					Person.customerList.get(i).addBill(bill);
				}
			}
		}
	}
	public void setFinance(){
		double revenue=0,expense=0;
		for(int i=0;i<Person.customerList.size();i++){
			for(int k=0;k<Person.customerList.get(i).getTicketList().size();k++){
				if(Person.customerList.get(i).getTicketList().get(k).getPaymentStatus().equals("Paid")){
					revenue += Person.customerList.get(i).getTicketList().get(k).getPrice();
				}
			}
		}
		for(int i=0;i<CreateFlight.flight.length;i++){
			expense += CreateFlight.flight[i].getFlightExpense();
		}
		Store.setRevenue(revenue);
		Store.setExpense(expense);
	}
	public static  String NoBracketNoSpace(String initial){
		String noBracket = initial.substring(1, initial.length()-1);
		String noSpace = noBracket.replace(" ", "");
		return noSpace;
	}
	
}

